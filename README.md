存放一些`kindle`的图书

有一些是免费搜集的, 有些是购买的. 希望能帮到你.

**数据存储及 CDN 下载都需要花费, 请按需下载**


# 程序

## 安全

* 黑客心理学社会工程学原理 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%BB%91%E5%AE%A2%E5%BF%83%E7%90%86%E5%AD%A6%E7%A4%BE%E4%BC%9A%E5%B7%A5%E7%A8%8B%E5%AD%A6%E5%8E%9F%E7%90%86.mobi)
* 黑客攻防技术宝典:Web实战篇(第2版) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%BB%91%E5%AE%A2%E6%94%BB%E9%98%B2%E6%8A%80%E6%9C%AF%E5%AE%9D%E5%85%B8%3AWeb%E5%AE%9E%E6%88%98%E7%AF%87(%E7%AC%AC2%E7%89%88).mobi)
* 黑客攻防技术宝典：浏览器实战篇 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%BB%91%E5%AE%A2%E6%94%BB%E9%98%B2%E6%8A%80%E6%9C%AF%E5%AE%9D%E5%85%B8%EF%BC%9A%E6%B5%8F%E8%A7%88%E5%99%A8%E5%AE%9E%E6%88%98%E7%AF%87.mobi)
* 欺骗的艺术 [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%AC%BA%E9%AA%97%E7%9A%84%E8%89%BA%E6%9C%AF.epub)    [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%AC%BA%E9%AA%97%E7%9A%84%E8%89%BA%E6%9C%AF.azw3)
* 黑客大曝\_恶意软件和Rootkit安全 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%BB%91%E5%AE%A2%E5%A4%A7%E6%9B%9D%E5%85%89_%E6%81%B6%E6%84%8F%E8%BD%AF%E4%BB%B6%E5%92%8CRootkit%E5%AE%89%E5%85%A8.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E9%BB%91%E5%AE%A2%E5%A4%A7%E6%9B%9D%E5%85%89_%E6%81%B6%E6%84%8F%E8%BD%AF%E4%BB%B6%E5%92%8CRootkit%E5%AE%89%E5%85%A8.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E9%BB%91%E5%AE%A2%E5%A4%A7%E6%9B%9D%E5%85%89_%E6%81%B6%E6%84%8F%E8%BD%AF%E4%BB%B6%E5%92%8CRootkit%E5%AE%89%E5%85%A8.azw3)
* 我知道你是谁，我知道你做过什么：隐私在社交网络时代的死亡 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E7%9F%A5%E9%81%93%E4%BD%A0%E6%98%AF%E8%B0%81%EF%BC%8C%E6%88%91%E7%9F%A5%E9%81%93%E4%BD%A0%E5%81%9A%E8%BF%87%E4%BB%80%E4%B9%88%EF%BC%9A%E9%9A%90%E7%A7%81%E5%9C%A8%E7%A4%BE%E4%BA%A4%E7%BD%91%E7%BB%9C%E6%97%B6%E4%BB%A3%E7%9A%84%E6%AD%BB%E4%BA%A1.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E7%9F%A5%E9%81%93%E4%BD%A0%E6%98%AF%E8%B0%81%EF%BC%8C%E6%88%91%E7%9F%A5%E9%81%93%E4%BD%A0%E5%81%9A%E8%BF%87%E4%BB%80%E4%B9%88%EF%BC%9A%E9%9A%90%E7%A7%81%E5%9C%A8%E7%A4%BE%E4%BA%A4%E7%BD%91%E7%BB%9C%E6%97%B6%E4%BB%A3%E7%9A%84%E6%AD%BB%E4%BA%A1.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E7%9F%A5%E9%81%93%E4%BD%A0%E6%98%AF%E8%B0%81%EF%BC%8C%E6%88%91%E7%9F%A5%E9%81%93%E4%BD%A0%E5%81%9A%E8%BF%87%E4%BB%80%E4%B9%88%EF%BC%9A%E9%9A%90%E7%A7%81%E5%9C%A8%E7%A4%BE%E4%BA%A4%E7%BD%91%E7%BB%9C%E6%97%B6%E4%BB%A3%E7%9A%84%E6%AD%BB%E4%BA%A1.azw3)
* 捍卫隐私 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%8D%8D%E5%8D%AB%E9%9A%90%E7%A7%81.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%8D%8D%E5%8D%AB%E9%9A%90%E7%A7%81.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%8D%8D%E5%8D%AB%E9%9A%90%E7%A7%81.azw3)

## 编程语言

### C

* C指针编程之道 [mobi](https://oss-blog.cdn.hujingnb.com/book/C%E6%8C%87%E9%92%88%E7%BC%96%E7%A8%8B%E4%B9%8B%E9%81%93.mobi)
* c语言程序设计实践教程第2版工业和信息化普通高等教育十二五规划立项项目21世纪高等教育计算机规划教材 [azw3](https://oss-blog.cdn.hujingnb.com/book/c%E8%AF%AD%E8%A8%80%E7%A8%8B%E5%BA%8F%E8%AE%BE%E8%AE%A1%E5%AE%9E%E8%B7%B5%E6%95%99%E7%A8%8B%E7%AC%AC2%E7%89%88%E5%B7%A5%E4%B8%9A%E5%92%8C%E4%BF%A1%E6%81%AF%E5%8C%96%E6%99%AE%E9%80%9A%E9%AB%98%E7%AD%89%E6%95%99%E8%82%B2%E5%8D%81%E4%BA%8C%E4%BA%94%E8%A7%84%E5%88%92%E7%AB%8B%E9%A1%B9%E9%A1%B9%E7%9B%AE21%E4%B8%96%E7%BA%AA%E9%AB%98%E7%AD%89%E6%95%99%E8%82%B2%E8%AE%A1%E7%AE%97%E6%9C%BA%E8%A7%84%E5%88%92%E6%95%99%E6%9D%90.azw3)
* 啊哈C语言！逻辑的挑战 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%95%8A%E5%93%88C%E8%AF%AD%E8%A8%80%EF%BC%81%E9%80%BB%E8%BE%91%E7%9A%84%E6%8C%91%E6%88%98.mobi)

### GO

* Go语言学习笔记 [mobi](https://oss-blog.cdn.hujingnb.com/book/Go%E8%AF%AD%E8%A8%80%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0.mobi)
* Go语言实战 [mobi](https://oss-blog.cdn.hujingnb.com/book/Go%E8%AF%AD%E8%A8%80%E5%AE%9E%E6%88%98.mobi)

### PHP

* php7内核剖析 [mobi](https://oss-blog.cdn.hujingnb.com/book/php7%E5%86%85%E6%A0%B8%E5%89%96%E6%9E%90.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/php7%E5%86%85%E6%A0%B8%E5%89%96%E6%9E%90.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/php7%E5%86%85%E6%A0%B8%E5%89%96%E6%9E%90.azw3)
* 细说PHP(第2版) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%86%E8%AF%B4PHP(%E7%AC%AC2%E7%89%88).mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%86%E8%AF%B4PHP(%E7%AC%AC2%E7%89%88).epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%86%E8%AF%B4PHP(%E7%AC%AC2%E7%89%88).azw3)
* Laravel框架关键技术解析 [mobi](https://oss-blog.cdn.hujingnb.com/book/Laravel%E6%A1%86%E6%9E%B6%E5%85%B3%E9%94%AE%E6%8A%80%E6%9C%AF%E8%A7%A3%E6%9E%90.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/Laravel%E6%A1%86%E6%9E%B6%E5%85%B3%E9%94%AE%E6%8A%80%E6%9C%AF%E8%A7%A3%E6%9E%90.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/Laravel%E6%A1%86%E6%9E%B6%E5%85%B3%E9%94%AE%E6%8A%80%E6%9C%AF%E8%A7%A3%E6%9E%90.azw3)

## 工具

### MySql

* MySQL技术内幕：SQL编程 [mobi](https://oss-blog.cdn.hujingnb.com/book/MySQL%E6%8A%80%E6%9C%AF%E5%86%85%E5%B9%95%EF%BC%9ASQL%E7%BC%96%E7%A8%8B.mobi)
* mysql性能调优与架构设计 [mobi](https://oss-blog.cdn.hujingnb.com/book/mysql%E6%80%A7%E8%83%BD%E8%B0%83%E4%BC%98%E4%B8%8E%E6%9E%B6%E6%9E%84%E8%AE%BE%E8%AE%A1.mobi)
* mysql技术内幕innodb存储引擎第2版 [mobi](https://oss-blog.cdn.hujingnb.com/book/mysql%E6%8A%80%E6%9C%AF%E5%86%85%E5%B9%95innodb%E5%AD%98%E5%82%A8%E5%BC%95%E6%93%8E%E7%AC%AC2%E7%89%88.mobi)
* mysql技术内幕第4版 [mobi](https://oss-blog.cdn.hujingnb.com/book/mysql%E6%8A%80%E6%9C%AF%E5%86%85%E5%B9%95%E7%AC%AC4%E7%89%88.mobi)
* 你不可不知的关系数据库理论异步图书 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BD%A0%E4%B8%8D%E5%8F%AF%E4%B8%8D%E7%9F%A5%E7%9A%84%E5%85%B3%E7%B3%BB%E6%95%B0%E6%8D%AE%E5%BA%93%E7%90%86%E8%AE%BA%E5%BC%82%E6%AD%A5%E5%9B%BE%E4%B9%A6.mobi)
* 高性能mysql第3版 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%AB%98%E6%80%A7%E8%83%BDmysql%E7%AC%AC3%E7%89%88.mobi)
* MySQL索引背后的数据结构及算法原理 [mobi](https://oss-blog.cdn.hujingnb.com/book/MySQL%E7%B4%A2%E5%BC%95%E8%83%8C%E5%90%8E%E7%9A%84%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E5%8F%8A%E7%AE%97%E6%B3%95%E5%8E%9F%E7%90%86.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/MySQL%E7%B4%A2%E5%BC%95%E8%83%8C%E5%90%8E%E7%9A%84%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E5%8F%8A%E7%AE%97%E6%B3%95%E5%8E%9F%E7%90%86.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/MySQL%E7%B4%A2%E5%BC%95%E8%83%8C%E5%90%8E%E7%9A%84%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E5%8F%8A%E7%AE%97%E6%B3%95%E5%8E%9F%E7%90%86.azw3)

### Redis

* Redis深度历险：核心原理与应用实践 [mobi](https://oss-blog.cdn.hujingnb.com/book/Redis%E6%B7%B1%E5%BA%A6%E5%8E%86%E9%99%A9%EF%BC%9A%E6%A0%B8%E5%BF%83%E5%8E%9F%E7%90%86%E4%B8%8E%E5%BA%94%E7%94%A8%E5%AE%9E%E8%B7%B5.mobi)

### hadoop

* hadoop应用开发技术详解大数据技术丛书 [mobi](https://oss-blog.cdn.hujingnb.com/book/hadoop%E5%BA%94%E7%94%A8%E5%BC%80%E5%8F%91%E6%8A%80%E6%9C%AF%E8%AF%A6%E8%A7%A3%E5%A4%A7%E6%95%B0%E6%8D%AE%E6%8A%80%E6%9C%AF%E4%B8%9B%E4%B9%A6.mobi)
* hadoop技术详解 [mobi](https://oss-blog.cdn.hujingnb.com/book/hadoop%E6%8A%80%E6%9C%AF%E8%AF%A6%E8%A7%A3.mobi)
* hadoop海量数据处理技术详解与项目实战 [mobi](https://oss-blog.cdn.hujingnb.com/book/hadoop%E6%B5%B7%E9%87%8F%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86%E6%8A%80%E6%9C%AF%E8%AF%A6%E8%A7%A3%E4%B8%8E%E9%A1%B9%E7%9B%AE%E5%AE%9E%E6%88%98.mobi)
* hbase原理与实践 [mobi](https://oss-blog.cdn.hujingnb.com/book/hbase%E5%8E%9F%E7%90%86%E4%B8%8E%E5%AE%9E%E8%B7%B5.mobi)

### Spark

* spark技术内幕深入解析spark内核架构设计与实现原理大数据技术丛书 [mobi](https://oss-blog.cdn.hujingnb.com/book/spark%E6%8A%80%E6%9C%AF%E5%86%85%E5%B9%95%E6%B7%B1%E5%85%A5%E8%A7%A3%E6%9E%90spark%E5%86%85%E6%A0%B8%E6%9E%B6%E6%9E%84%E8%AE%BE%E8%AE%A1%E4%B8%8E%E5%AE%9E%E7%8E%B0%E5%8E%9F%E7%90%86%E5%A4%A7%E6%95%B0%E6%8D%AE%E6%8A%80%E6%9C%AF%E4%B8%9B%E4%B9%A6.mobi)

### Nginx

* 深入理解Nginx：模块开发与架构解析（第2版）[mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3Nginx%EF%BC%9A%E6%A8%A1%E5%9D%97%E5%BC%80%E5%8F%91%E4%B8%8E%E6%9E%B6%E6%9E%84%E8%A7%A3%E6%9E%90%EF%BC%88%E7%AC%AC2%E7%89%88%EF%BC%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3Nginx%EF%BC%9A%E6%A8%A1%E5%9D%97%E5%BC%80%E5%8F%91%E4%B8%8E%E6%9E%B6%E6%9E%84%E8%A7%A3%E6%9E%90%EF%BC%88%E7%AC%AC2%E7%89%88%EF%BC%89.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3Nginx%EF%BC%9A%E6%A8%A1%E5%9D%97%E5%BC%80%E5%8F%91%E4%B8%8E%E6%9E%B6%E6%9E%84%E8%A7%A3%E6%9E%90%EF%BC%88%E7%AC%AC2%E7%89%88%EF%BC%89.azw3)

## 算法

* 可能与不可能的边界-P.NP问题趣史 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%8F%AF%E8%83%BD%E4%B8%8E%E4%B8%8D%E5%8F%AF%E8%83%BD%E7%9A%84%E8%BE%B9%E7%95%8C-P.NP%E9%97%AE%E9%A2%98%E8%B6%A3%E5%8F%B2.mobi)
* 思考的乐趣 Matrix67数学笔记 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%80%9D%E8%80%83%E7%9A%84%E4%B9%90%E8%B6%A3%20Matrix67%E6%95%B0%E5%AD%A6%E7%AC%94%E8%AE%B0.mobi)
* 算法与数据中台 基于 Google、Facebook与微博实践 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%E4%B8%8E%E6%95%B0%E6%8D%AE%E4%B8%AD%E5%8F%B0%20%E5%9F%BA%E4%BA%8E%20Google%E3%80%81Facebook%E4%B8%8E%E5%BE%AE%E5%8D%9A%E5%AE%9E%E8%B7%B5.mobi) [pdf](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%E4%B8%8E%E6%95%B0%E6%8D%AE%E4%B8%AD%E5%8F%B0%20%E5%9F%BA%E4%BA%8E%20Google%E3%80%81Facebook%E4%B8%8E%E5%BE%AE%E5%8D%9A%E5%AE%9E%E8%B7%B5.pdf)
* 算法神探：一部谷歌首席工程师写的CS小说 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%E7%A5%9E%E6%8E%A2%EF%BC%9A%E4%B8%80%E9%83%A8%E8%B0%B7%E6%AD%8C%E9%A6%96%E5%B8%AD%E5%B7%A5%E7%A8%8B%E5%B8%88%E5%86%99%E7%9A%84CS%E5%B0%8F%E8%AF%B4.mobi)
* 算法精解 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%E7%B2%BE%E8%A7%A3.mobi)
* 这就是搜索引擎核心技术详解 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%BF%99%E5%B0%B1%E6%98%AF%E6%90%9C%E7%B4%A2%E5%BC%95%E6%93%8E%E6%A0%B8%E5%BF%83%E6%8A%80%E6%9C%AF%E8%AF%A6%E8%A7%A3.mobi)
* 迷茫的旅行商：一个无处不在的计算机算法问题 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%BF%B7%E8%8C%AB%E7%9A%84%E6%97%85%E8%A1%8C%E5%95%86%EF%BC%9A%E4%B8%80%E4%B8%AA%E6%97%A0%E5%A4%84%E4%B8%8D%E5%9C%A8%E7%9A%84%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%AE%97%E6%B3%95%E9%97%AE%E9%A2%98.mobi)
* 阿里巴巴B2B电商算法实践 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B4B2B%E7%94%B5%E5%95%86%E7%AE%97%E6%B3%95%E5%AE%9E%E8%B7%B5.mobi)    [pdf](https://oss-blog.cdn.hujingnb.com/book/%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B4B2B%E7%94%B5%E5%95%86%E7%AE%97%E6%B3%95%E5%AE%9E%E8%B7%B5.pdf)
* 算法的陷阱 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%E7%9A%84%E9%99%B7%E9%98%B1.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%E7%9A%84%E9%99%B7%E9%98%B1.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%E7%9A%84%E9%99%B7%E9%98%B1.azw3)
* 算法（第4版） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%EF%BC%88%E7%AC%AC4%E7%89%88%EF%BC%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%EF%BC%88%E7%AC%AC4%E7%89%88%EF%BC%89.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%EF%BC%88%E7%AC%AC4%E7%89%88%EF%BC%89.azw3)
* 算法的乐趣 (图灵原创) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%E7%9A%84%E4%B9%90%E8%B6%A3%20(%E5%9B%BE%E7%81%B5%E5%8E%9F%E5%88%9B).mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%E7%9A%84%E4%B9%90%E8%B6%A3%20(%E5%9B%BE%E7%81%B5%E5%8E%9F%E5%88%9B).epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%97%E6%B3%95%E7%9A%84%E4%B9%90%E8%B6%A3%20(%E5%9B%BE%E7%81%B5%E5%8E%9F%E5%88%9B).azw3)


## 操作系统

* 操作系统导论 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F%E5%AF%BC%E8%AE%BA.mobi)
* 操作系统真象还原 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F%E7%9C%9F%E8%B1%A1%E8%BF%98%E5%8E%9F.mobi)
* 深入linux内核架构 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5linux%E5%86%85%E6%A0%B8%E6%9E%B6%E6%9E%84.mobi)
* unix编程艺术 [azw3](https://oss-blog.cdn.hujingnb.com/book/unix%E7%BC%96%E7%A8%8B%E8%89%BA%E6%9C%AF.azw3)
* 鸟哥的Linux私房菜——基础学习篇（第四版） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%B8%9F%E5%93%A5%E7%9A%84Linux%E7%A7%81%E6%88%BF%E8%8F%9C%E2%80%94%E2%80%94%E5%9F%BA%E7%A1%80%E5%AD%A6%E4%B9%A0%E7%AF%87%EF%BC%88%E7%AC%AC%E5%9B%9B%E7%89%88%EF%BC%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E9%B8%9F%E5%93%A5%E7%9A%84Linux%E7%A7%81%E6%88%BF%E8%8F%9C%E2%80%94%E2%80%94%E5%9F%BA%E7%A1%80%E5%AD%A6%E4%B9%A0%E7%AF%87%EF%BC%88%E7%AC%AC%E5%9B%9B%E7%89%88%EF%BC%89.epub)  [azw3](https://oss-blog.cdn.hujingnb.com/book/%E9%B8%9F%E5%93%A5%E7%9A%84Linux%E7%A7%81%E6%88%BF%E8%8F%9C%E2%80%94%E2%80%94%E5%9F%BA%E7%A1%80%E5%AD%A6%E4%B9%A0%E7%AF%87%EF%BC%88%E7%AC%AC%E5%9B%9B%E7%89%88%EF%BC%89.azw3)
* 鸟哥的Linux私房菜：服务器架设篇(第3版) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%B8%9F%E5%93%A5%E7%9A%84Linux%E7%A7%81%E6%88%BF%E8%8F%9C%EF%BC%9A%E6%9C%8D%E5%8A%A1%E5%99%A8%E6%9E%B6%E8%AE%BE%E7%AF%87(%E7%AC%AC3%E7%89%88).mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E9%B8%9F%E5%93%A5%E7%9A%84Linux%E7%A7%81%E6%88%BF%E8%8F%9C%EF%BC%9A%E6%9C%8D%E5%8A%A1%E5%99%A8%E6%9E%B6%E8%AE%BE%E7%AF%87(%E7%AC%AC3%E7%89%88).epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E9%B8%9F%E5%93%A5%E7%9A%84Linux%E7%A7%81%E6%88%BF%E8%8F%9C%EF%BC%9A%E6%9C%8D%E5%8A%A1%E5%99%A8%E6%9E%B6%E8%AE%BE%E7%AF%87(%E7%AC%AC3%E7%89%88).azw3)
* 深入理解计算机系统 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%B3%BB%E7%BB%9F.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%B3%BB%E7%BB%9F.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%B3%BB%E7%BB%9F.azw3)


## 计算机网络

* wireshark网络分析的艺术信息安全技术丛书 [mobi](https://oss-blog.cdn.hujingnb.com/book/wireshark%E7%BD%91%E7%BB%9C%E5%88%86%E6%9E%90%E7%9A%84%E8%89%BA%E6%9C%AF%E4%BF%A1%E6%81%AF%E5%AE%89%E5%85%A8%E6%8A%80%E6%9C%AF%E4%B8%9B%E4%B9%A6.mobi)
* 图解TCPIP(第5版) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%9B%BE%E8%A7%A3TCPIP(%E7%AC%AC5%E7%89%88).mobi)
* 网络是怎样连接的 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BD%91%E7%BB%9C%E6%98%AF%E6%80%8E%E6%A0%B7%E8%BF%9E%E6%8E%A5%E7%9A%84.mobi)
* 计算机网络（第7版） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BD%91%E7%BB%9C%EF%BC%88%E7%AC%AC7%E7%89%88%EF%BC%89.mobi)
* 零信任网络：在不可信网络中构建安全系统 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%9B%B6%E4%BF%A1%E4%BB%BB%E7%BD%91%E7%BB%9C%EF%BC%9A%E5%9C%A8%E4%B8%8D%E5%8F%AF%E4%BF%A1%E7%BD%91%E7%BB%9C%E4%B8%AD%E6%9E%84%E5%BB%BA%E5%AE%89%E5%85%A8%E7%B3%BB%E7%BB%9F%EF%BC%88%E5%BC%82%E6%AD%A5%E5%9B%BE%E4%B9%A6%EF%BC%89.mobi)
* 深入理解计算机网络 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BD%91%E7%BB%9C.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BD%91%E7%BB%9C.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BD%91%E7%BB%9C.azw3)
* 5G：开启未来无线通信创新之路 [mobi](https://oss-blog.cdn.hujingnb.com/book/5G%EF%BC%9A%E5%BC%80%E5%90%AF%E6%9C%AA%E6%9D%A5%E6%97%A0%E7%BA%BF%E9%80%9A%E4%BF%A1%E5%88%9B%E6%96%B0%E4%B9%8B%E8%B7%AF.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/5G%EF%BC%9A%E5%BC%80%E5%90%AF%E6%9C%AA%E6%9D%A5%E6%97%A0%E7%BA%BF%E9%80%9A%E4%BF%A1%E5%88%9B%E6%96%B0%E4%B9%8B%E8%B7%AF.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/5G%EF%BC%9A%E5%BC%80%E5%90%AF%E6%9C%AA%E6%9D%A5%E6%97%A0%E7%BA%BF%E9%80%9A%E4%BF%A1%E5%88%9B%E6%96%B0%E4%B9%8B%E8%B7%AF.azw3)



## 计算机原理

* 七周七并发模型 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%83%E5%91%A8%E4%B8%83%E5%B9%B6%E5%8F%91%E6%A8%A1%E5%9E%8B.mobi)
* 七周七数据库 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%83%E5%91%A8%E4%B8%83%E6%95%B0%E6%8D%AE%E5%BA%93.mobi)
* 七周七语言：理解多种编程范型 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%83%E5%91%A8%E4%B8%83%E8%AF%AD%E8%A8%80%EF%BC%9A%E7%90%86%E8%A7%A3%E5%A4%9A%E7%A7%8D%E7%BC%96%E7%A8%8B%E8%8C%83%E5%9E%8B.mobi)
* 两周自制脚本语言 (图灵程序设计丛书) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%A4%E5%91%A8%E8%87%AA%E5%88%B6%E8%84%9A%E6%9C%AC%E8%AF%AD%E8%A8%80%20(%E5%9B%BE%E7%81%B5%E7%A8%8B%E5%BA%8F%E8%AE%BE%E8%AE%A1%E4%B8%9B%E4%B9%A6).mobi)
* 深入理解并行编程 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E7%90%86%E8%A7%A3%E5%B9%B6%E8%A1%8C%E7%BC%96%E7%A8%8B.mobi)
* 码书编码与解码的战争 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%A0%81%E4%B9%A6%E7%BC%96%E7%A0%81%E4%B8%8E%E8%A7%A3%E7%A0%81%E7%9A%84%E6%88%98%E4%BA%89.mobi)
* 程序是怎样跑起来的 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%A8%8B%E5%BA%8F%E6%98%AF%E6%80%8E%E6%A0%B7%E8%B7%91%E8%B5%B7%E6%9D%A5%E7%9A%84.mobi)
* 编码隐匿在计算机软硬件背后的语言 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BC%96%E7%A0%81%E9%9A%90%E5%8C%BF%E5%9C%A8%E8%AE%A1%E7%AE%97%E6%9C%BA%E8%BD%AF%E7%A1%AC%E4%BB%B6%E8%83%8C%E5%90%8E%E7%9A%84%E8%AF%AD%E8%A8%80.mobi)
* 计算机是怎样跑起来的 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%AE%A1%E7%AE%97%E6%9C%BA%E6%98%AF%E6%80%8E%E6%A0%B7%E8%B7%91%E8%B5%B7%E6%9D%A5%E7%9A%84.mobi)
* 计算机程序的构造和解释 （原书第2版） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%A8%8B%E5%BA%8F%E7%9A%84%E6%9E%84%E9%80%A0%E5%92%8C%E8%A7%A3%E9%87%8A%20%EF%BC%88%E5%8E%9F%E4%B9%A6%E7%AC%AC2%E7%89%88%EF%BC%89.mobi)
* 深入浅出SSD：固态存储核心技术、原理与实战 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E6%B5%85%E5%87%BASSD%EF%BC%9A%E5%9B%BA%E6%80%81%E5%AD%98%E5%82%A8%E6%A0%B8%E5%BF%83%E6%8A%80%E6%9C%AF%E3%80%81%E5%8E%9F%E7%90%86%E4%B8%8E%E5%AE%9E%E6%88%98.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E6%B5%85%E5%87%BASSD%EF%BC%9A%E5%9B%BA%E6%80%81%E5%AD%98%E5%82%A8%E6%A0%B8%E5%BF%83%E6%8A%80%E6%9C%AF%E3%80%81%E5%8E%9F%E7%90%86%E4%B8%8E%E5%AE%9E%E6%88%98.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E6%B5%85%E5%87%BASSD%EF%BC%9A%E5%9B%BA%E6%80%81%E5%AD%98%E5%82%A8%E6%A0%B8%E5%BF%83%E6%8A%80%E6%9C%AF%E3%80%81%E5%8E%9F%E7%90%86%E4%B8%8E%E5%AE%9E%E6%88%98.azw3)
* 老码识途\_从机器码到框架的系统观逆向修炼之路 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%80%81%E7%A0%81%E8%AF%86%E9%80%94_%E4%BB%8E%E6%9C%BA%E5%99%A8%E7%A0%81%E5%88%B0%E6%A1%86%E6%9E%B6%E7%9A%84%E7%B3%BB%E7%BB%9F%E8%A7%82%E9%80%86%E5%90%91%E4%BF%AE%E7%82%BC%E4%B9%8B%E8%B7%AF.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%80%81%E7%A0%81%E8%AF%86%E9%80%94_%E4%BB%8E%E6%9C%BA%E5%99%A8%E7%A0%81%E5%88%B0%E6%A1%86%E6%9E%B6%E7%9A%84%E7%B3%BB%E7%BB%9F%E8%A7%82%E9%80%86%E5%90%91%E4%BF%AE%E7%82%BC%E4%B9%8B%E8%B7%AF.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E8%80%81%E7%A0%81%E8%AF%86%E9%80%94_%E4%BB%8E%E6%9C%BA%E5%99%A8%E7%A0%81%E5%88%B0%E6%A1%86%E6%9E%B6%E7%9A%84%E7%B3%BB%E7%BB%9F%E8%A7%82%E9%80%86%E5%90%91%E4%BF%AE%E7%82%BC%E4%B9%8B%E8%B7%AF.azw3)
* 深入浅出通信原理 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E6%B5%85%E5%87%BA%E9%80%9A%E4%BF%A1%E5%8E%9F%E7%90%86.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E6%B5%85%E5%87%BA%E9%80%9A%E4%BF%A1%E5%8E%9F%E7%90%86.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%85%A5%E6%B5%85%E5%87%BA%E9%80%9A%E4%BF%A1%E5%8E%9F%E7%90%86.azw3)
* 丹尼尔·希利斯讲计算机 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%B9%E5%B0%BC%E5%B0%94%C2%B7%E5%B8%8C%E5%88%A9%E6%96%AF%E8%AE%B2%E8%AE%A1%E7%AE%97%E6%9C%BA.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%B9%E5%B0%BC%E5%B0%94%C2%B7%E5%B8%8C%E5%88%A9%E6%96%AF%E8%AE%B2%E8%AE%A1%E7%AE%97%E6%9C%BA.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%B9%E5%B0%BC%E5%B0%94%C2%B7%E5%B8%8C%E5%88%A9%E6%96%AF%E8%AE%B2%E8%AE%A1%E7%AE%97%E6%9C%BA.azw3)

## 编码修炼

* 代码之美 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%A3%E7%A0%81%E4%B9%8B%E7%BE%8E.mobi)    [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%A3%E7%A0%81%E4%B9%8B%E7%BE%8E.epub)
* 代码之髓：编程语言核心概念 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%A3%E7%A0%81%E4%B9%8B%E9%AB%93%EF%BC%9A%E7%BC%96%E7%A8%8B%E8%AF%AD%E8%A8%80%E6%A0%B8%E5%BF%83%E6%A6%82%E5%BF%B5.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%A3%E7%A0%81%E4%B9%8B%E9%AB%93%EF%BC%9A%E7%BC%96%E7%A8%8B%E8%AF%AD%E8%A8%80%E6%A0%B8%E5%BF%83%E6%A6%82%E5%BF%B5.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%A3%E7%A0%81%E4%B9%8B%E9%AB%93%EF%BC%9A%E7%BC%96%E7%A8%8B%E8%AF%AD%E8%A8%80%E6%A0%B8%E5%BF%83%E6%A6%82%E5%BF%B5.azw3)
* 代码精进之路从码农到工匠 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%A3%E7%A0%81%E7%B2%BE%E8%BF%9B%E4%B9%8B%E8%B7%AF%E4%BB%8E%E7%A0%81%E5%86%9C%E5%88%B0%E5%B7%A5%E5%8C%A0.mobi)
* 修改软件的艺术构建易维护代码的9条最佳实践 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BF%AE%E6%94%B9%E8%BD%AF%E4%BB%B6%E7%9A%84%E8%89%BA%E6%9C%AF%E6%9E%84%E5%BB%BA%E6%98%93%E7%BB%B4%E6%8A%A4%E4%BB%A3%E7%A0%81%E7%9A%849%E6%9D%A1%E6%9C%80%E4%BD%B3%E5%AE%9E%E8%B7%B5.mobi)
* 大型网站技术架构核心原理与案例分析 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A7%E5%9E%8B%E7%BD%91%E7%AB%99%E6%8A%80%E6%9C%AF%E6%9E%B6%E6%9E%84%E6%A0%B8%E5%BF%83%E5%8E%9F%E7%90%86%E4%B8%8E%E6%A1%88%E4%BE%8B%E5%88%86%E6%9E%90.mobi)
* 实现模式修订版软件开发方法学精选系列 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%AE%9E%E7%8E%B0%E6%A8%A1%E5%BC%8F%E4%BF%AE%E8%AE%A2%E7%89%88%E8%BD%AF%E4%BB%B6%E5%BC%80%E5%8F%91%E6%96%B9%E6%B3%95%E5%AD%A6%E7%B2%BE%E9%80%89%E7%B3%BB%E5%88%97.mobi)
* 性能之巅 洞悉系统、企业与云计算 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%80%A7%E8%83%BD%E4%B9%8B%E5%B7%85%20%E6%B4%9E%E6%82%89%E7%B3%BB%E7%BB%9F%E3%80%81%E4%BC%81%E4%B8%9A%E4%B8%8E%E4%BA%91%E8%AE%A1%E7%AE%97.mobi)
* 挑战编程技能57道程序员功力测试题 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%8C%91%E6%88%98%E7%BC%96%E7%A8%8B%E6%8A%80%E8%83%BD57%E9%81%93%E7%A8%8B%E5%BA%8F%E5%91%98%E5%8A%9F%E5%8A%9B%E6%B5%8B%E8%AF%95%E9%A2%98.mobi)
* 架构整洁之道 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9E%B6%E6%9E%84%E6%95%B4%E6%B4%81%E4%B9%8B%E9%81%93.mobi) [pdf](https://oss-blog.cdn.hujingnb.com/book/%E6%9E%B6%E6%9E%84%E6%95%B4%E6%B4%81%E4%B9%8B%E9%81%93.pdf)
* 编写可读代码的艺术 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BC%96%E5%86%99%E5%8F%AF%E8%AF%BB%E4%BB%A3%E7%A0%81%E7%9A%84%E8%89%BA%E6%9C%AF.mobi)
* 编程的原则 改善代码质量的101个方法 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BC%96%E7%A8%8B%E7%9A%84%E5%8E%9F%E5%88%99%20%E6%94%B9%E5%96%84%E4%BB%A3%E7%A0%81%E8%B4%A8%E9%87%8F%E7%9A%84101%E4%B8%AA%E6%96%B9%E6%B3%95.mobi) [pdf](https://oss-blog.cdn.hujingnb.com/book/%E7%BC%96%E7%A8%8B%E7%9A%84%E5%8E%9F%E5%88%99%20%E6%94%B9%E5%96%84%E4%BB%A3%E7%A0%81%E8%B4%A8%E9%87%8F%E7%9A%84101%E4%B8%AA%E6%96%B9%E6%B3%95.pdf)
* 重构改善既有代码的设计第2版 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%87%8D%E6%9E%84%E6%94%B9%E5%96%84%E6%97%A2%E6%9C%89%E4%BB%A3%E7%A0%81%E7%9A%84%E8%AE%BE%E8%AE%A1%E7%AC%AC2%E7%89%88.mobi)
* 编程之美-微软技术面试心得 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BC%96%E7%A8%8B%E4%B9%8B%E7%BE%8E-%E5%BE%AE%E8%BD%AF%E6%8A%80%E6%9C%AF%E9%9D%A2%E8%AF%95%E5%BF%83%E5%BE%97.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%BC%96%E7%A8%8B%E4%B9%8B%E7%BE%8E-%E5%BE%AE%E8%BD%AF%E6%8A%80%E6%9C%AF%E9%9D%A2%E8%AF%95%E5%BF%83%E5%BE%97.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%BC%96%E7%A8%8B%E4%B9%8B%E7%BE%8E-%E5%BE%AE%E8%BD%AF%E6%8A%80%E6%9C%AF%E9%9D%A2%E8%AF%95%E5%BF%83%E5%BE%97.azw3)
* 程序原本 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%A8%8B%E5%BA%8F%E5%8E%9F%E6%9C%AC.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%A8%8B%E5%BA%8F%E5%8E%9F%E6%9C%AC.epub)
* 我的架构思想 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E7%9A%84%E6%9E%B6%E6%9E%84%E6%80%9D%E6%83%B3.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E7%9A%84%E6%9E%B6%E6%9E%84%E6%80%9D%E6%83%B3.epub)
* 程序员必读之软件架构 [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%A8%8B%E5%BA%8F%E5%91%98%E5%BF%85%E8%AF%BB%E4%B9%8B%E8%BD%AF%E4%BB%B6%E6%9E%B6%E6%9E%84.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%A8%8B%E5%BA%8F%E5%91%98%E5%BF%85%E8%AF%BB%E4%B9%8B%E8%BD%AF%E4%BB%B6%E6%9E%B6%E6%9E%84.azw3)
* 架构师之路（58沈剑） [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%9E%B6%E6%9E%84%E5%B8%88%E4%B9%8B%E8%B7%AF%EF%BC%8858%E6%B2%88%E5%89%91%EF%BC%89.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%9E%B6%E6%9E%84%E5%B8%88%E4%B9%8B%E8%B7%AF%EF%BC%8858%E6%B2%88%E5%89%91%EF%BC%89.azw3)
* 微服务治理 体系、架构及实践 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%BE%AE%E6%9C%8D%E5%8A%A1%E6%B2%BB%E7%90%86%20%E4%BD%93%E7%B3%BB%E3%80%81%E6%9E%B6%E6%9E%84%E5%8F%8A%E5%AE%9E%E8%B7%B5.mobi)
* 微服务设计 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%BE%AE%E6%9C%8D%E5%8A%A1%E8%AE%BE%E8%AE%A1.mobi)
* 技术人的百宝黑皮书(阿里巴巴2020技术年货) [pdf](https://oss-blog.cdn.hujingnb.com/book/%E6%8A%80%E6%9C%AF%E4%BA%BA%E7%9A%84%E7%99%BE%E5%AE%9D%E9%BB%91%E7%9A%AE%E4%B9%A6(%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B42020%E6%8A%80%E6%9C%AF%E5%B9%B4%E8%B4%A7).pdf)
* 编程格调（异步图书） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BC%96%E7%A8%8B%E6%A0%BC%E8%B0%83%EF%BC%88%E5%BC%82%E6%AD%A5%E5%9B%BE%E4%B9%A6%EF%BC%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%BC%96%E7%A8%8B%E6%A0%BC%E8%B0%83%EF%BC%88%E5%BC%82%E6%AD%A5%E5%9B%BE%E4%B9%A6%EF%BC%89.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%BC%96%E7%A8%8B%E6%A0%BC%E8%B0%83%EF%BC%88%E5%BC%82%E6%AD%A5%E5%9B%BE%E4%B9%A6%EF%BC%89.azw3)
* 自制搜索引擎 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%87%AA%E5%88%B6%E6%90%9C%E7%B4%A2%E5%BC%95%E6%93%8E.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%87%AA%E5%88%B6%E6%90%9C%E7%B4%A2%E5%BC%95%E6%93%8E.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E8%87%AA%E5%88%B6%E6%90%9C%E7%B4%A2%E5%BC%95%E6%93%8E.azw3)

## 项目

* 大道至易：实践者的思想（第二版） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A7%E9%81%93%E8%87%B3%E6%98%93%EF%BC%9A%E5%AE%9E%E8%B7%B5%E8%80%85%E7%9A%84%E6%80%9D%E6%83%B3%EF%BC%88%E7%AC%AC%E4%BA%8C%E7%89%88%EF%BC%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A7%E9%81%93%E8%87%B3%E6%98%93%EF%BC%9A%E5%AE%9E%E8%B7%B5%E8%80%85%E7%9A%84%E6%80%9D%E6%83%B3%EF%BC%88%E7%AC%AC%E4%BA%8C%E7%89%88%EF%BC%89.epub)
* 重新定义团队谷歌如何工作 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%87%8D%E6%96%B0%E5%AE%9A%E4%B9%89%E5%9B%A2%E9%98%9F%E8%B0%B7%E6%AD%8C%E5%A6%82%E4%BD%95%E5%B7%A5%E4%BD%9C.mobi)
* 项目管理修炼之道 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%A1%B9%E7%9B%AE%E7%AE%A1%E7%90%86%E4%BF%AE%E7%82%BC%E4%B9%8B%E9%81%93.mobi)
* 网易一千零一夜 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BD%91%E6%98%93%E4%B8%80%E5%8D%83%E9%9B%B6%E4%B8%80%E5%A4%9C.mobi)
* 构建之法现代软件工程 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9E%84%E5%BB%BA%E4%B9%8B%E6%B3%95%E7%8E%B0%E4%BB%A3%E8%BD%AF%E4%BB%B6%E5%B7%A5%E7%A8%8B.mobi)
* 卓有成效的管理者 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%8D%93%E6%9C%89%E6%88%90%E6%95%88%E7%9A%84%E7%AE%A1%E7%90%86%E8%80%85.mobi)
* 人月神话 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E6%9C%88%E7%A5%9E%E8%AF%9D.mobi)
* 敏捷软件开发原则模式与实践c版 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%95%8F%E6%8D%B7%E8%BD%AF%E4%BB%B6%E5%BC%80%E5%8F%91%E5%8E%9F%E5%88%99%E6%A8%A1%E5%BC%8F%E4%B8%8E%E5%AE%9E%E8%B7%B5c%E7%89%88.mobi)
* 奔跑吧程序员从零开始打造产品技术和团队 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A5%94%E8%B7%91%E5%90%A7%E7%A8%8B%E5%BA%8F%E5%91%98%E4%BB%8E%E9%9B%B6%E5%BC%80%E5%A7%8B%E6%89%93%E9%80%A0%E4%BA%A7%E5%93%81%E6%8A%80%E6%9C%AF%E5%92%8C%E5%9B%A2%E9%98%9F.mobi)
* 部落的力量 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%83%A8%E8%90%BD%E7%9A%84%E5%8A%9B%E9%87%8F.mobi)
* 重新理解创业-一个创业者的途中思考 [epub](https://oss-blog.cdn.hujingnb.com/book/%E9%87%8D%E6%96%B0%E7%90%86%E8%A7%A3%E5%88%9B%E4%B8%9A_%E4%B8%80%E4%B8%AA%E5%88%9B%E4%B8%9A%E8%80%85%E7%9A%84%E9%80%94%E4%B8%AD%E6%80%9D%E8%80%83.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%87%8D%E6%96%B0%E7%90%86%E8%A7%A3%E5%88%9B%E4%B8%9A_%E4%B8%80%E4%B8%AA%E5%88%9B%E4%B8%9A%E8%80%85%E7%9A%84%E9%80%94%E4%B8%AD%E6%80%9D%E8%80%83.mobi)
* 重来更为简单有效的商业思维 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%87%8D%E6%9D%A5%E6%9B%B4%E4%B8%BA%E7%AE%80%E5%8D%95%E6%9C%89%E6%95%88%E7%9A%84%E5%95%86%E4%B8%9A%E6%80%9D%E7%BB%B4.mobi)
* 重来2更为简单高效的远程工作方式 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%87%8D%E6%9D%A52%E6%9B%B4%E4%B8%BA%E7%AE%80%E5%8D%95%E9%AB%98%E6%95%88%E7%9A%84%E8%BF%9C%E7%A8%8B%E5%B7%A5%E4%BD%9C%E6%96%B9%E5%BC%8F.mobi)
* 重来3跳出疯狂的忙碌如果可以重来你还会做工作狂么财经作家吴晓波推荐一个名为慢公司时代的来临 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%87%8D%E6%9D%A53%E8%B7%B3%E5%87%BA%E7%96%AF%E7%8B%82%E7%9A%84%E5%BF%99%E7%A2%8C%E5%A6%82%E6%9E%9C%E5%8F%AF%E4%BB%A5%E9%87%8D%E6%9D%A5%E4%BD%A0%E8%BF%98%E4%BC%9A%E5%81%9A%E5%B7%A5%E4%BD%9C%E7%8B%82%E4%B9%88%E8%B4%A2%E7%BB%8F%E4%BD%9C%E5%AE%B6%E5%90%B4%E6%99%93%E6%B3%A2%E6%8E%A8%E8%8D%90%E4%B8%80%E4%B8%AA%E5%90%8D%E4%B8%BA%E6%85%A2%E5%85%AC%E5%8F%B8%E6%97%B6%E4%BB%A3%E7%9A%84%E6%9D%A5%E4%B8%B4.mobi)
* 门后的秘密卓越管理的故事 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%97%A8%E5%90%8E%E7%9A%84%E7%A7%98%E5%AF%86%E5%8D%93%E8%B6%8A%E7%AE%A1%E7%90%86%E7%9A%84%E6%95%85%E4%BA%8B.mobi)
* 创业维艰 [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%88%9B%E4%B8%9A%E7%BB%B4%E8%89%B0.epub)	[mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%88%9B%E4%B8%9A%E7%BB%B4%E8%89%B0.mobi)
* 精益企业高效能组织如何规模化创新静益系列 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%B2%BE%E7%9B%8A%E4%BC%81%E4%B8%9A%E9%AB%98%E6%95%88%E8%83%BD%E7%BB%84%E7%BB%87%E5%A6%82%E4%BD%95%E8%A7%84%E6%A8%A1%E5%8C%96%E5%88%9B%E6%96%B0%E9%9D%99%E7%9B%8A%E7%B3%BB%E5%88%97.mobi)
* 精益创业如何建立一个精悍可持续可赢利的公司 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%B2%BE%E7%9B%8A%E5%88%9B%E4%B8%9A%E5%A6%82%E4%BD%95%E5%BB%BA%E7%AB%8B%E4%B8%80%E4%B8%AA%E7%B2%BE%E6%82%8D%E5%8F%AF%E6%8C%81%E7%BB%AD%E5%8F%AF%E8%B5%A2%E5%88%A9%E7%9A%84%E5%85%AC%E5%8F%B8.mobi)
* 精益创业实战第2版 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%B2%BE%E7%9B%8A%E5%88%9B%E4%B8%9A%E5%AE%9E%E6%88%98%E7%AC%AC2%E7%89%88.mobi)
* 别想那只大象-乔治莱考夫 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%88%AB%E6%83%B3%E9%82%A3%E5%8F%AA%E5%A4%A7%E8%B1%A1-%E4%B9%94%E6%B2%BB%E8%8E%B1%E8%80%83%E5%A4%AB.mobi) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%88%AB%E6%83%B3%E9%82%A3%E5%8F%AA%E5%A4%A7%E8%B1%A1-%E4%B9%94%E6%B2%BB%E8%8E%B1%E8%80%83%E5%A4%AB.azw3)
* 创业无畏：指数级成长路线图 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%88%9B%E4%B8%9A%E6%97%A0%E7%95%8F%EF%BC%9A%E6%8C%87%E6%95%B0%E7%BA%A7%E6%88%90%E9%95%BF%E8%B7%AF%E7%BA%BF%E5%9B%BE.mobi)   [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%88%9B%E4%B8%9A%E6%97%A0%E7%95%8F%EF%BC%9A%E6%8C%87%E6%95%B0%E7%BA%A7%E6%88%90%E9%95%BF%E8%B7%AF%E7%BA%BF%E5%9B%BE.azw3)
* Google工作整理术（解读版） [mobi](https://oss-blog.cdn.hujingnb.com/book/Google%E5%B7%A5%E4%BD%9C%E6%95%B4%E7%90%86%E6%9C%AF%EF%BC%88%E8%A7%A3%E8%AF%BB%E7%89%88%EF%BC%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/Google%E5%B7%A5%E4%BD%9C%E6%95%B4%E7%90%86%E6%9C%AF%EF%BC%88%E8%A7%A3%E8%AF%BB%E7%89%88%EF%BC%89.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/Google%E5%B7%A5%E4%BD%9C%E6%95%B4%E7%90%86%E6%9C%AF%EF%BC%88%E8%A7%A3%E8%AF%BB%E7%89%88%EF%BC%89.azw3)

## 产品

* 上瘾让用户养成使用习惯的四大产品逻辑互联网产品设计行为设计纯干货 [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%8A%E7%98%BE%E8%AE%A9%E7%94%A8%E6%88%B7%E5%85%BB%E6%88%90%E4%BD%BF%E7%94%A8%E4%B9%A0%E6%83%AF%E7%9A%84%E5%9B%9B%E5%A4%A7%E4%BA%A7%E5%93%81%E9%80%BB%E8%BE%91%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E8%AE%BE%E8%AE%A1%E8%A1%8C%E4%B8%BA%E8%AE%BE%E8%AE%A1%E7%BA%AF%E5%B9%B2%E8%B4%A7.azw3)
* 人人都是产品经理2.0——写给泛产品经理 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E4%BA%BA%E9%83%BD%E6%98%AF%E4%BA%A7%E5%93%81%E7%BB%8F%E7%90%862.0%E2%80%94%E2%80%94%E5%86%99%E7%BB%99%E6%B3%9B%E4%BA%A7%E5%93%81%E7%BB%8F%E7%90%86.mobi)
* 结网：改变世界的互联网产品经理 [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%93%E7%BD%91%EF%BC%9A%E6%94%B9%E5%8F%98%E4%B8%96%E7%95%8C%E7%9A%84%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E7%BB%8F%E7%90%86.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%93%E7%BD%91%EF%BC%9A%E6%94%B9%E5%8F%98%E4%B8%96%E7%95%8C%E7%9A%84%E4%BA%92%E8%81%94%E7%BD%91%E4%BA%A7%E5%93%81%E7%BB%8F%E7%90%86.mobi)
* 从零开始做产品经理产品经理的第一本书 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E9%9B%B6%E5%BC%80%E5%A7%8B%E5%81%9A%E4%BA%A7%E5%93%81%E7%BB%8F%E7%90%86%E4%BA%A7%E5%93%81%E7%BB%8F%E7%90%86%E7%9A%84%E7%AC%AC%E4%B8%80%E6%9C%AC%E4%B9%A6.mobi) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E9%9B%B6%E5%BC%80%E5%A7%8B%E5%81%9A%E4%BA%A7%E5%93%81%E7%BB%8F%E7%90%86%E4%BA%A7%E5%93%81%E7%BB%8F%E7%90%86%E7%9A%84%E7%AC%AC%E4%B8%80%E6%9C%AC%E4%B9%A6.azw3)
* 畅销的原理为什么好观念、好产品会一炮而红 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%95%85%E9%94%80%E7%9A%84%E5%8E%9F%E7%90%86%E4%B8%BA%E4%BB%80%E4%B9%88%E5%A5%BD%E8%A7%82%E5%BF%B5%E3%80%81%E5%A5%BD%E4%BA%A7%E5%93%81%E4%BC%9A%E4%B8%80%E7%82%AE%E8%80%8C%E7%BA%A2.mobi)   [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%95%85%E9%94%80%E7%9A%84%E5%8E%9F%E7%90%86%E4%B8%BA%E4%BB%80%E4%B9%88%E5%A5%BD%E8%A7%82%E5%BF%B5%E3%80%81%E5%A5%BD%E4%BA%A7%E5%93%81%E4%BC%9A%E4%B8%80%E7%82%AE%E8%80%8C%E7%BA%A2.epub)   [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%95%85%E9%94%80%E7%9A%84%E5%8E%9F%E7%90%86%E4%B8%BA%E4%BB%80%E4%B9%88%E5%A5%BD%E8%A7%82%E5%BF%B5%E3%80%81%E5%A5%BD%E4%BA%A7%E5%93%81%E4%BC%9A%E4%B8%80%E7%82%AE%E8%80%8C%E7%BA%A2.azw3)
* 一生的旅程：迪士尼CEO自述批量打造超级IP的经营哲学 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%80%E7%94%9F%E7%9A%84%E6%97%85%E7%A8%8B%EF%BC%9A%E8%BF%AA%E5%A3%AB%E5%B0%BCCEO%E8%87%AA%E8%BF%B0%E6%89%B9%E9%87%8F%E6%89%93%E9%80%A0%E8%B6%85%E7%BA%A7IP%E7%9A%84%E7%BB%8F%E8%90%A5%E5%93%B2%E5%AD%A6.mobi) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%80%E7%94%9F%E7%9A%84%E6%97%85%E7%A8%8B%EF%BC%9A%E8%BF%AA%E5%A3%AB%E5%B0%BCCEO%E8%87%AA%E8%BF%B0%E6%89%B9%E9%87%8F%E6%89%93%E9%80%A0%E8%B6%85%E7%BA%A7IP%E7%9A%84%E7%BB%8F%E8%90%A5%E5%93%B2%E5%AD%A6.azw3) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%80%E7%94%9F%E7%9A%84%E6%97%85%E7%A8%8B%EF%BC%9A%E8%BF%AA%E5%A3%AB%E5%B0%BCCEO%E8%87%AA%E8%BF%B0%E6%89%B9%E9%87%8F%E6%89%93%E9%80%A0%E8%B6%85%E7%BA%A7IP%E7%9A%84%E7%BB%8F%E8%90%A5%E5%93%B2%E5%AD%A6.epub) [pdf](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%80%E7%94%9F%E7%9A%84%E6%97%85%E7%A8%8B%EF%BC%9A%E8%BF%AA%E5%A3%AB%E5%B0%BCCEO%E8%87%AA%E8%BF%B0%E6%89%B9%E9%87%8F%E6%89%93%E9%80%A0%E8%B6%85%E7%BA%A7IP%E7%9A%84%E7%BB%8F%E8%90%A5%E5%93%B2%E5%AD%A6.pdf)
* 超级IP：互联网新物种方法论 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E7%BA%A7IP%EF%BC%9A%E4%BA%92%E8%81%94%E7%BD%91%E6%96%B0%E7%89%A9%E7%A7%8D%E6%96%B9%E6%B3%95%E8%AE%BA.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E7%BA%A7IP%EF%BC%9A%E4%BA%92%E8%81%94%E7%BD%91%E6%96%B0%E7%89%A9%E7%A7%8D%E6%96%B9%E6%B3%95%E8%AE%BA.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E7%BA%A7IP%EF%BC%9A%E4%BA%92%E8%81%94%E7%BD%91%E6%96%B0%E7%89%A9%E7%A7%8D%E6%96%B9%E6%B3%95%E8%AE%BA.epub)
* 凭什么让你充值：一个游戏策划的自我修养 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%87%AD%E4%BB%80%E4%B9%88%E8%AE%A9%E4%BD%A0%E5%85%85%E5%80%BC%EF%BC%9A%E4%B8%80%E4%B8%AA%E6%B8%B8%E6%88%8F%E7%AD%96%E5%88%92%E7%9A%84%E8%87%AA%E6%88%91%E4%BF%AE%E5%85%BB.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%87%AD%E4%BB%80%E4%B9%88%E8%AE%A9%E4%BD%A0%E5%85%85%E5%80%BC%EF%BC%9A%E4%B8%80%E4%B8%AA%E6%B8%B8%E6%88%8F%E7%AD%96%E5%88%92%E7%9A%84%E8%87%AA%E6%88%91%E4%BF%AE%E5%85%BB.epub)


## 程序人生

* 代码之外的功夫程序员精进之路 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%A3%E7%A0%81%E4%B9%8B%E5%A4%96%E7%9A%84%E5%8A%9F%E5%A4%AB%E7%A8%8B%E5%BA%8F%E5%91%98%E7%B2%BE%E8%BF%9B%E4%B9%8B%E8%B7%AF.mobi)
* 代码的未来 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%A3%E7%A0%81%E7%9A%84%E6%9C%AA%E6%9D%A5.mobi)
* 华为传（2020） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%8D%8E%E4%B8%BA%E4%BC%A0%EF%BC%882020%EF%BC%89.mobi)
* 大头侃人：任正非 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A7%E5%A4%B4%E4%BE%83%E4%BA%BA%EF%BC%9A%E4%BB%BB%E6%AD%A3%E9%9D%9E.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A7%E5%A4%B4%E4%BE%83%E4%BA%BA%EF%BC%9A%E4%BB%BB%E6%AD%A3%E9%9D%9E.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A7%E5%A4%B4%E4%BE%83%E4%BA%BA%EF%BC%9A%E4%BB%BB%E6%AD%A3%E9%9D%9E.azw3)
* 只是为了好玩 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%8F%AA%E6%98%AF%E4%B8%BA%E4%BA%86%E5%A5%BD%E7%8E%A9.mobi)
* 哥德尔艾舍尔巴赫集异璧之大成 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%93%A5%E5%BE%B7%E5%B0%94%E8%89%BE%E8%88%8D%E5%B0%94%E5%B7%B4%E8%B5%AB%E9%9B%86%E5%BC%82%E7%92%A7%E4%B9%8B%E5%A4%A7%E6%88%90.mobi)
* 图灵的秘密他的生平思想及论文解读 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%9B%BE%E7%81%B5%E7%9A%84%E7%A7%98%E5%AF%86%E4%BB%96%E7%9A%84%E7%94%9F%E5%B9%B3%E6%80%9D%E6%83%B3%E5%8F%8A%E8%AE%BA%E6%96%87%E8%A7%A3%E8%AF%BB.mobi)
* 尽在双11：阿里巴巴技术演进与超越 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%B0%BD%E5%9C%A8%E5%8F%8C11%EF%BC%9A%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B4%E6%8A%80%E6%9C%AF%E6%BC%94%E8%BF%9B%E4%B8%8E%E8%B6%85%E8%B6%8A.mobi)
* 逆流而上：阿里巴巴技术成长之路 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%80%86%E6%B5%81%E8%80%8C%E4%B8%8A%EF%BC%9A%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B4%E6%8A%80%E6%9C%AF%E6%88%90%E9%95%BF%E4%B9%8B%E8%B7%AF.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E9%80%86%E6%B5%81%E8%80%8C%E4%B8%8A%EF%BC%9A%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B4%E6%8A%80%E6%9C%AF%E6%88%90%E9%95%BF%E4%B9%8B%E8%B7%AF.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E9%80%86%E6%B5%81%E8%80%8C%E4%B8%8A%EF%BC%9A%E9%98%BF%E9%87%8C%E5%B7%B4%E5%B7%B4%E6%8A%80%E6%9C%AF%E6%88%90%E9%95%BF%E4%B9%8B%E8%B7%AF.azw3)
* 有效需求分析 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9C%89%E6%95%88%E9%9C%80%E6%B1%82%E5%88%86%E6%9E%90.mobi)
* 李开复自传：世界因你不同 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9D%8E%E5%BC%80%E5%A4%8D%E8%87%AA%E4%BC%A0%EF%BC%9A%E4%B8%96%E7%95%8C%E5%9B%A0%E4%BD%A0%E4%B8%8D%E5%90%8C.mobi)
* 松本行弘的程序世界 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9D%BE%E6%9C%AC%E8%A1%8C%E5%BC%98%E7%9A%84%E7%A8%8B%E5%BA%8F%E4%B8%96%E7%95%8C.mobi)
* 浪潮之巅 [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%B5%AA%E6%BD%AE%E4%B9%8B%E5%B7%85.epub)    [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B5%AA%E6%BD%AE%E4%B9%8B%E5%B7%85.mobi)
* 淘宝技术这十年 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%98%E5%AE%9D%E6%8A%80%E6%9C%AF%E8%BF%99%E5%8D%81%E5%B9%B4.mobi)
* 程序员的思维修炼开发认知潜能的九堂课 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%A8%8B%E5%BA%8F%E5%91%98%E7%9A%84%E6%80%9D%E7%BB%B4%E4%BF%AE%E7%82%BC%E5%BC%80%E5%8F%91%E8%AE%A4%E7%9F%A5%E6%BD%9C%E8%83%BD%E7%9A%84%E4%B9%9D%E5%A0%82%E8%AF%BE.mobi)
* 程序员的自我修养 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%A8%8B%E5%BA%8F%E5%91%98%E7%9A%84%E8%87%AA%E6%88%91%E4%BF%AE%E5%85%BB.mobi)
* 艾伦·图灵传 [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%89%BE%E4%BC%A6%C2%B7%E5%9B%BE%E7%81%B5%E4%BC%A0.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%89%BE%E4%BC%A6%C2%B7%E5%9B%BE%E7%81%B5%E4%BC%A0.mobi)
* 若为自由故 自由软件之父理查德斯托曼传 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%8B%A5%E4%B8%BA%E8%87%AA%E7%94%B1%E6%95%85%20%E8%87%AA%E7%94%B1%E8%BD%AF%E4%BB%B6%E4%B9%8B%E7%88%B6%E7%90%86%E6%9F%A5%E5%BE%B7%E6%96%AF%E6%89%98%E6%9B%BC%E4%BC%A0.mobi)
* 软件故事：谁发明了那些经典的编程语言 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%BD%AF%E4%BB%B6%E6%95%85%E4%BA%8B%EF%BC%9A%E8%B0%81%E5%8F%91%E6%98%8E%E4%BA%86%E9%82%A3%E4%BA%9B%E7%BB%8F%E5%85%B8%E7%9A%84%E7%BC%96%E7%A8%8B%E8%AF%AD%E8%A8%80.mobi)
* 软件项目估算 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%BD%AF%E4%BB%B6%E9%A1%B9%E7%9B%AE%E4%BC%B0%E7%AE%97.mobi) [pdf](https://oss-blog.cdn.hujingnb.com/book/%E8%BD%AF%E4%BB%B6%E9%A1%B9%E7%9B%AE%E4%BC%B0%E7%AE%97.pdf)
* 软技能代码之外的生存指南 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%BD%AF%E6%8A%80%E8%83%BD%E4%BB%A3%E7%A0%81%E4%B9%8B%E5%A4%96%E7%9A%84%E7%94%9F%E5%AD%98%E6%8C%87%E5%8D%97.mobi)
* 黑客与画家 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%BB%91%E5%AE%A2%E4%B8%8E%E7%94%BB%E5%AE%B6.mobi)
* 硅谷之火（第3版） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%A1%85%E8%B0%B7%E4%B9%8B%E7%81%AB%EF%BC%88%E7%AC%AC3%E7%89%88%EF%BC%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%A1%85%E8%B0%B7%E4%B9%8B%E7%81%AB%EF%BC%88%E7%AC%AC3%E7%89%88%EF%BC%89.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%A1%85%E8%B0%B7%E4%B9%8B%E7%81%AB%EF%BC%88%E7%AC%AC3%E7%89%88%EF%BC%89.azw3)

## 其他

* 暗网 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9A%97%E7%BD%91.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%9A%97%E7%BD%91.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%9A%97%E7%BD%91.azw3)
* 我是一个程序员 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E6%98%AF%E4%B8%80%E4%B8%AA%E7%A8%8B%E5%BA%8F%E5%91%98.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E6%98%AF%E4%B8%80%E4%B8%AA%E7%A8%8B%E5%BA%8F%E5%91%98.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E6%98%AF%E4%B8%80%E4%B8%AA%E7%A8%8B%E5%BA%8F%E5%91%98.azw3)
* 在搜索引擎首页露脸的N种方法 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%A8%E6%90%9C%E7%B4%A2%E5%BC%95%E6%93%8E%E9%A6%96%E9%A1%B5%E9%9C%B2%E8%84%B8%E7%9A%84N%E7%A7%8D%E6%96%B9%E6%B3%95.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%A8%E6%90%9C%E7%B4%A2%E5%BC%95%E6%93%8E%E9%A6%96%E9%A1%B5%E9%9C%B2%E8%84%B8%E7%9A%84N%E7%A7%8D%E6%96%B9%E6%B3%95.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%A8%E6%90%9C%E7%B4%A2%E5%BC%95%E6%93%8E%E9%A6%96%E9%A1%B5%E9%9C%B2%E8%84%B8%E7%9A%84N%E7%A7%8D%E6%96%B9%E6%B3%95.azw3)
* 如何创造可信的AI [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E5%88%9B%E9%80%A0%E5%8F%AF%E4%BF%A1%E7%9A%84AI.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E5%88%9B%E9%80%A0%E5%8F%AF%E4%BF%A1%E7%9A%84AI.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E5%88%9B%E9%80%A0%E5%8F%AF%E4%BF%A1%E7%9A%84AI.azw3)
* 失控的真相 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%B1%E6%8E%A7%E7%9A%84%E7%9C%9F%E7%9B%B8.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%B1%E6%8E%A7%E7%9A%84%E7%9C%9F%E7%9B%B8.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%B1%E6%8E%A7%E7%9A%84%E7%9C%9F%E7%9B%B8.azw3)
* 不会被机器替代的人：智能时代的生存策略 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%8D%E4%BC%9A%E8%A2%AB%E6%9C%BA%E5%99%A8%E6%9B%BF%E4%BB%A3%E7%9A%84%E4%BA%BA%EF%BC%9A%E6%99%BA%E8%83%BD%E6%97%B6%E4%BB%A3%E7%9A%84%E7%94%9F%E5%AD%98%E7%AD%96%E7%95%A5.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%8D%E4%BC%9A%E8%A2%AB%E6%9C%BA%E5%99%A8%E6%9B%BF%E4%BB%A3%E7%9A%84%E4%BA%BA%EF%BC%9A%E6%99%BA%E8%83%BD%E6%97%B6%E4%BB%A3%E7%9A%84%E7%94%9F%E5%AD%98%E7%AD%96%E7%95%A5.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%8D%E4%BC%9A%E8%A2%AB%E6%9C%BA%E5%99%A8%E6%9B%BF%E4%BB%A3%E7%9A%84%E4%BA%BA%EF%BC%9A%E6%99%BA%E8%83%BD%E6%97%B6%E4%BB%A3%E7%9A%84%E7%94%9F%E5%AD%98%E7%AD%96%E7%95%A5.azw3)

# 学术

## 数学

* 从一到无穷大 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E4%B8%80%E5%88%B0%E6%97%A0%E7%A9%B7%E5%A4%A7.mobi)
* 数学之美 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%95%B0%E5%AD%A6%E4%B9%8B%E7%BE%8E.mobi)
* 数学也荒唐：20个脑洞大开的数学趣题 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%95%B0%E5%AD%A6%E4%B9%9F%E8%8D%92%E5%94%90%EF%BC%9A20%E4%B8%AA%E8%84%91%E6%B4%9E%E5%A4%A7%E5%BC%80%E7%9A%84%E6%95%B0%E5%AD%A6%E8%B6%A3%E9%A2%98.mobi)
* 10堂极简概率课 [mobi](https://oss-blog.cdn.hujingnb.com/book/10%E5%A0%82%E6%9E%81%E7%AE%80%E6%A6%82%E7%8E%87%E8%AF%BE.mobi)
* 天才与算法人脑与ai的数学思维 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A9%E6%89%8D%E4%B8%8E%E7%AE%97%E6%B3%95%E4%BA%BA%E8%84%91%E4%B8%8Eai%E7%9A%84%E6%95%B0%E5%AD%A6%E6%80%9D%E7%BB%B4.mobi)
* 随机漫步的傻瓜 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%9A%8F%E6%9C%BA%E6%BC%AB%E6%AD%A5%E7%9A%84%E5%82%BB%E7%93%9C.mobi)
* 费马大定理：一个困惑了世间智者358年的谜 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%B4%B9%E9%A9%AC%E5%A4%A7%E5%AE%9A%E7%90%86%EF%BC%9A%E4%B8%80%E4%B8%AA%E5%9B%B0%E6%83%91%E4%BA%86%E4%B8%96%E9%97%B4%E6%99%BA%E8%80%85358%E5%B9%B4%E7%9A%84%E8%B0%9C.mobi)
* 具体数学：计算机科学基础（第2版） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%85%B7%E4%BD%93%E6%95%B0%E5%AD%A6%EF%BC%9A%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%A7%91%E5%AD%A6%E5%9F%BA%E7%A1%80%EF%BC%88%E7%AC%AC2%E7%89%88%EF%BC%89.mobi)
* 数学也荒唐：20个脑洞大开的数学趣题 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%95%B0%E5%AD%A6%E4%B9%9F%E8%8D%92%E5%94%90%EF%BC%9A20%E4%B8%AA%E8%84%91%E6%B4%9E%E5%A4%A7%E5%BC%80%E7%9A%84%E6%95%B0%E5%AD%A6%E8%B6%A3%E9%A2%98.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%95%B0%E5%AD%A6%E4%B9%9F%E8%8D%92%E5%94%90%EF%BC%9A20%E4%B8%AA%E8%84%91%E6%B4%9E%E5%A4%A7%E5%BC%80%E7%9A%84%E6%95%B0%E5%AD%A6%E8%B6%A3%E9%A2%98.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%95%B0%E5%AD%A6%E4%B9%9F%E8%8D%92%E5%94%90%EF%BC%9A20%E4%B8%AA%E8%84%91%E6%B4%9E%E5%A4%A7%E5%BC%80%E7%9A%84%E6%95%B0%E5%AD%A6%E8%B6%A3%E9%A2%98.azw3)
* 大问题 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A7%E9%97%AE%E9%A2%98.mobi)
* 烧掉数学书 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%83%A7%E6%8E%89%E6%95%B0%E5%AD%A6%E4%B9%A6.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%83%A7%E6%8E%89%E6%95%B0%E5%AD%A6%E4%B9%A6.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%83%A7%E6%8E%89%E6%95%B0%E5%AD%A6%E4%B9%A6.azw3)
* 莎士比亚的零 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%8E%8E%E5%A3%AB%E6%AF%94%E4%BA%9A%E7%9A%84%E9%9B%B6.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%8E%8E%E5%A3%AB%E6%AF%94%E4%BA%9A%E7%9A%84%E9%9B%B6.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E8%8E%8E%E5%A3%AB%E6%AF%94%E4%BA%9A%E7%9A%84%E9%9B%B6.azw3)
* 上帝创造整数 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%8A%E5%B8%9D%E5%88%9B%E9%80%A0%E6%95%B4%E6%95%B0.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%8A%E5%B8%9D%E5%88%9B%E9%80%A0%E6%95%B4%E6%95%B0.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%8A%E5%B8%9D%E5%88%9B%E9%80%A0%E6%95%B4%E6%95%B0.azw3)
* 别拿相关当因果！因果关系简易入门 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%88%AB%E6%8B%BF%E7%9B%B8%E5%85%B3%E5%BD%93%E5%9B%A0%E6%9E%9C%EF%BC%81%E5%9B%A0%E6%9E%9C%E5%85%B3%E7%B3%BB%E7%AE%80%E6%98%93%E5%85%A5%E9%97%A8.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%88%AB%E6%8B%BF%E7%9B%B8%E5%85%B3%E5%BD%93%E5%9B%A0%E6%9E%9C%EF%BC%81%E5%9B%A0%E6%9E%9C%E5%85%B3%E7%B3%BB%E7%AE%80%E6%98%93%E5%85%A5%E9%97%A8.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%88%AB%E6%8B%BF%E7%9B%B8%E5%85%B3%E5%BD%93%E5%9B%A0%E6%9E%9C%EF%BC%81%E5%9B%A0%E6%9E%9C%E5%85%B3%E7%B3%BB%E7%AE%80%E6%98%93%E5%85%A5%E9%97%A8.azw3)
* 悖论：破解科学史上最复杂的9大谜团 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%82%96%E8%AE%BA%EF%BC%9A%E7%A0%B4%E8%A7%A3%E7%A7%91%E5%AD%A6%E5%8F%B2%E4%B8%8A%E6%9C%80%E5%A4%8D%E6%9D%82%E7%9A%849%E5%A4%A7%E8%B0%9C%E5%9B%A2.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%82%96%E8%AE%BA%EF%BC%9A%E7%A0%B4%E8%A7%A3%E7%A7%91%E5%AD%A6%E5%8F%B2%E4%B8%8A%E6%9C%80%E5%A4%8D%E6%9D%82%E7%9A%849%E5%A4%A7%E8%B0%9C%E5%9B%A2.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%82%96%E8%AE%BA%EF%BC%9A%E7%A0%B4%E8%A7%A3%E7%A7%91%E5%AD%A6%E5%8F%B2%E4%B8%8A%E6%9C%80%E5%A4%8D%E6%9D%82%E7%9A%849%E5%A4%A7%E8%B0%9C%E5%9B%A2.azw3)
* 博弈论(一切博弈论的源起之作) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%8D%9A%E5%BC%88%E8%AE%BA(%E4%B8%80%E5%88%87%E5%8D%9A%E5%BC%88%E8%AE%BA%E7%9A%84%E6%BA%90%E8%B5%B7%E4%B9%8B%E4%BD%9C).mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%8D%9A%E5%BC%88%E8%AE%BA(%E4%B8%80%E5%88%87%E5%8D%9A%E5%BC%88%E8%AE%BA%E7%9A%84%E6%BA%90%E8%B5%B7%E4%B9%8B%E4%BD%9C).epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%8D%9A%E5%BC%88%E8%AE%BA(%E4%B8%80%E5%88%87%E5%8D%9A%E5%BC%88%E8%AE%BA%E7%9A%84%E6%BA%90%E8%B5%B7%E4%B9%8B%E4%BD%9C).azw3)




## 历史

* 明朝那些事儿 [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%98%8E%E6%9C%9D%E9%82%A3%E4%BA%9B%E4%BA%8B%E5%84%BF.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%98%8E%E6%9C%9D%E9%82%A3%E4%BA%9B%E4%BA%8B%E5%84%BF.mobi)
* 起源：万物大历史 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%B5%B7%E6%BA%90%EF%BC%9A%E4%B8%87%E7%89%A9%E5%A4%A7%E5%8E%86%E5%8F%B2.mobi) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E8%B5%B7%E6%BA%90%EF%BC%9A%E4%B8%87%E7%89%A9%E5%A4%A7%E5%8E%86%E5%8F%B2.azw3) [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%B5%B7%E6%BA%90%EF%BC%9A%E4%B8%87%E7%89%A9%E5%A4%A7%E5%8E%86%E5%8F%B2.epub)
* 地球编年史1第十二个天体 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%B0%E7%90%83%E7%BC%96%E5%B9%B4%E5%8F%B21%E7%AC%AC%E5%8D%81%E4%BA%8C%E4%B8%AA%E5%A4%A9%E4%BD%93.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%B0%E7%90%83%E7%BC%96%E5%B9%B4%E5%8F%B21%E7%AC%AC%E5%8D%81%E4%BA%8C%E4%B8%AA%E5%A4%A9%E4%BD%93.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%B0%E7%90%83%E7%BC%96%E5%B9%B4%E5%8F%B21%E7%AC%AC%E5%8D%81%E4%BA%8C%E4%B8%AA%E5%A4%A9%E4%BD%93.azw3)
* 地球编年史2通往天国的阶梯 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%B0%E7%90%83%E7%BC%96%E5%B9%B4%E5%8F%B22%E9%80%9A%E5%BE%80%E5%A4%A9%E5%9B%BD%E7%9A%84%E9%98%B6%E6%A2%AF.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%B0%E7%90%83%E7%BC%96%E5%B9%B4%E5%8F%B22%E9%80%9A%E5%BE%80%E5%A4%A9%E5%9B%BD%E7%9A%84%E9%98%B6%E6%A2%AF.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%B0%E7%90%83%E7%BC%96%E5%B9%B4%E5%8F%B22%E9%80%9A%E5%BE%80%E5%A4%A9%E5%9B%BD%E7%9A%84%E9%98%B6%E6%A2%AF.azw3)
* 地球编年史3众神与人类的战争 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%B0%E7%90%83%E7%BC%96%E5%B9%B4%E5%8F%B23%E4%BC%97%E7%A5%9E%E4%B8%8E%E4%BA%BA%E7%B1%BB%E7%9A%84%E6%88%98%E4%BA%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%B0%E7%90%83%E7%BC%96%E5%B9%B4%E5%8F%B23%E4%BC%97%E7%A5%9E%E4%B8%8E%E4%BA%BA%E7%B1%BB%E7%9A%84%E6%88%98%E4%BA%89.epub)

## 物理

* 物理真好玩 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%89%A9%E7%90%86%E7%9C%9F%E5%A5%BD%E7%8E%A9.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%89%A9%E7%90%86%E7%9C%9F%E5%A5%BD%E7%8E%A9.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%89%A9%E7%90%86%E7%9C%9F%E5%A5%BD%E7%8E%A9.azw3)
* 《三体》中的物理学 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E3%80%8A%E4%B8%89%E4%BD%93%E3%80%8B%E4%B8%AD%E7%9A%84%E7%89%A9%E7%90%86%E5%AD%A6.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E3%80%8A%E4%B8%89%E4%BD%93%E3%80%8B%E4%B8%AD%E7%9A%84%E7%89%A9%E7%90%86%E5%AD%A6.epub)

## 天文

* 在别的星球上 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%A8%E5%88%AB%E7%9A%84%E6%98%9F%E7%90%83%E4%B8%8A.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%A8%E5%88%AB%E7%9A%84%E6%98%9F%E7%90%83%E4%B8%8A.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%9C%A8%E5%88%AB%E7%9A%84%E6%98%9F%E7%90%83%E4%B8%8A.azw3)

## 学习

* 二手时间 [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%8C%E6%89%8B%E6%97%B6%E9%97%B4.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%8C%E6%89%8B%E6%97%B6%E9%97%B4.mobi)
* 如何再次拿起书 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E5%86%8D%E6%AC%A1%E6%8B%BF%E8%B5%B7%E4%B9%A6.mobi)
* 如何把事情做到最好 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E6%8A%8A%E4%BA%8B%E6%83%85%E5%81%9A%E5%88%B0%E6%9C%80%E5%A5%BD.mobi)
* 终身学习：10个你必须掌握的未来生存法则 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%88%E8%BA%AB%E5%AD%A6%E4%B9%A0%EF%BC%9A10%E4%B8%AA%E4%BD%A0%E5%BF%85%E9%A1%BB%E6%8E%8C%E6%8F%A1%E7%9A%84%E6%9C%AA%E6%9D%A5%E7%94%9F%E5%AD%98%E6%B3%95%E5%88%99.mobi)
* 从行动开始 [.mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E8%A1%8C%E5%8A%A8%E5%BC%80%E5%A7%8B.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E8%A1%8C%E5%8A%A8%E5%BC%80%E5%A7%8B.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E8%A1%8C%E5%8A%A8%E5%BC%80%E5%A7%8B.azw3)
* 超效率手册-99个史上更全面的时间管理技巧 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E6%95%88%E7%8E%87%E6%89%8B%E5%86%8C-99%E4%B8%AA%E5%8F%B2%E4%B8%8A%E6%9B%B4%E5%85%A8%E9%9D%A2%E7%9A%84%E6%97%B6%E9%97%B4%E7%AE%A1%E7%90%86%E6%8A%80%E5%B7%A7.mobi)
* 麦肯锡方法 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%BA%A6%E8%82%AF%E9%94%A1%E6%96%B9%E6%B3%95.mobi)
* 如何阅读一本书 [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E9%98%85%E8%AF%BB%E4%B8%80%E6%9C%AC%E4%B9%A6.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E9%98%85%E8%AF%BB%E4%B8%80%E6%9C%AC%E4%B9%A6.mobi)
* 你骨子里是个牛人 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BD%A0%E9%AA%A8%E5%AD%90%E9%87%8C%E6%98%AF%E4%B8%AA%E7%89%9B%E4%BA%BA.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BD%A0%E9%AA%A8%E5%AD%90%E9%87%8C%E6%98%AF%E4%B8%AA%E7%89%9B%E4%BA%BA.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BD%A0%E9%AA%A8%E5%AD%90%E9%87%8C%E6%98%AF%E4%B8%AA%E7%89%9B%E4%BA%BA.azw3)
* 好习惯的秘密 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A5%BD%E4%B9%A0%E6%83%AF%E7%9A%84%E7%A7%98%E5%AF%86.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A5%BD%E4%B9%A0%E6%83%AF%E7%9A%84%E7%A7%98%E5%AF%86.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A5%BD%E4%B9%A0%E6%83%AF%E7%9A%84%E7%A7%98%E5%AF%86.azw3)

## 心理

* 蛤蟆先生去看心理医生 [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%9B%A4%E8%9F%86%E5%85%88%E7%94%9F%E5%8E%BB%E7%9C%8B%E5%BF%83%E7%90%86%E5%8C%BB%E7%94%9F.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%9B%A4%E8%9F%86%E5%85%88%E7%94%9F%E5%8E%BB%E7%9C%8B%E5%BF%83%E7%90%86%E5%8C%BB%E7%94%9F.mobi)
* 社交恐惧心理学 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%A4%BE%E4%BA%A4%E6%81%90%E6%83%A7%E5%BF%83%E7%90%86%E5%AD%A6.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%A4%BE%E4%BA%A4%E6%81%90%E6%83%A7%E5%BF%83%E7%90%86%E5%AD%A6.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%A4%BE%E4%BA%A4%E6%81%90%E6%83%A7%E5%BF%83%E7%90%86%E5%AD%A6.azw3)
* 网络心理学 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BD%91%E7%BB%9C%E5%BF%83%E7%90%86%E5%AD%A6.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%BD%91%E7%BB%9C%E5%BF%83%E7%90%86%E5%AD%A6.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%BD%91%E7%BB%9C%E5%BF%83%E7%90%86%E5%AD%A6.azw3)
* 网络心理学：隐藏在现象背后的行为设计真相 [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%BD%91%E7%BB%9C%E5%BF%83%E7%90%86%E5%AD%A6%EF%BC%9A%E9%9A%90%E8%97%8F%E5%9C%A8%E7%8E%B0%E8%B1%A1%E8%83%8C%E5%90%8E%E7%9A%84%E8%A1%8C%E4%B8%BA%E8%AE%BE%E8%AE%A1%E7%9C%9F%E7%9B%B8.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%BD%91%E7%BB%9C%E5%BF%83%E7%90%86%E5%AD%A6%EF%BC%9A%E9%9A%90%E8%97%8F%E5%9C%A8%E7%8E%B0%E8%B1%A1%E8%83%8C%E5%90%8E%E7%9A%84%E8%A1%8C%E4%B8%BA%E8%AE%BE%E8%AE%A1%E7%9C%9F%E7%9B%B8.azw3)
* 爆笑吧！心理学大神来了 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%88%86%E7%AC%91%E5%90%A7%EF%BC%81%E5%BF%83%E7%90%86%E5%AD%A6%E5%A4%A7%E7%A5%9E%E6%9D%A5%E4%BA%86.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%88%86%E7%AC%91%E5%90%A7%EF%BC%81%E5%BF%83%E7%90%86%E5%AD%A6%E5%A4%A7%E7%A5%9E%E6%9D%A5%E4%BA%86.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%88%86%E7%AC%91%E5%90%A7%EF%BC%81%E5%BF%83%E7%90%86%E5%AD%A6%E5%A4%A7%E7%A5%9E%E6%9D%A5%E4%BA%86.azw3)
* 天才在左疯子在右：心理疾病漫谈 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A9%E6%89%8D%E5%9C%A8%E5%B7%A6%E7%96%AF%E5%AD%90%E5%9C%A8%E5%8F%B3%EF%BC%9A%E5%BF%83%E7%90%86%E7%96%BE%E7%97%85%E6%BC%AB%E8%B0%88.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A9%E6%89%8D%E5%9C%A8%E5%B7%A6%E7%96%AF%E5%AD%90%E5%9C%A8%E5%8F%B3%EF%BC%9A%E5%BF%83%E7%90%86%E7%96%BE%E7%97%85%E6%BC%AB%E8%B0%88.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A9%E6%89%8D%E5%9C%A8%E5%B7%A6%E7%96%AF%E5%AD%90%E5%9C%A8%E5%8F%B3%EF%BC%9A%E5%BF%83%E7%90%86%E7%96%BE%E7%97%85%E6%BC%AB%E8%B0%88.azw3)


## 其他

* 时间旅行简史 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%97%B6%E9%97%B4%E6%97%85%E8%A1%8C%E7%AE%80%E5%8F%B2.mobi)
* 未来简史 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9C%AA%E6%9D%A5%E7%AE%80%E5%8F%B2.mobi)
* 人类简史 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E7%B1%BB%E7%AE%80%E5%8F%B2.mobi)
* 数学简史 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%95%B0%E5%AD%A6%E7%AE%80%E5%8F%B2.mobi)
* 我们为什么要睡觉 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E4%BB%AC%E4%B8%BA%E4%BB%80%E4%B9%88%E8%A6%81%E7%9D%A1%E8%A7%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E4%BB%AC%E4%B8%BA%E4%BB%80%E4%B9%88%E8%A6%81%E7%9D%A1%E8%A7%89.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E4%BB%AC%E4%B8%BA%E4%BB%80%E4%B9%88%E8%A6%81%E7%9D%A1%E8%A7%89.azw3) [pdf](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E4%BB%AC%E4%B8%BA%E4%BB%80%E4%B9%88%E8%A6%81%E7%9D%A1%E8%A7%89.pdf)
* 浪漫优雅的古巴比伦文明话说世界 [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%B5%AA%E6%BC%AB%E4%BC%98%E9%9B%85%E7%9A%84%E5%8F%A4%E5%B7%B4%E6%AF%94%E4%BC%A6%E6%96%87%E6%98%8E%E8%AF%9D%E8%AF%B4%E4%B8%96%E7%95%8C.epub)
* 贫穷的本质我们为什么摆脱不了贫穷 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%B4%AB%E7%A9%B7%E7%9A%84%E6%9C%AC%E8%B4%A8%E6%88%91%E4%BB%AC%E4%B8%BA%E4%BB%80%E4%B9%88%E6%91%86%E8%84%B1%E4%B8%8D%E4%BA%86%E8%B4%AB%E7%A9%B7.mobi)
* 真相：十个理由告诉你我们错看了世界 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%9C%9F%E7%9B%B8%EF%BC%9A%E5%8D%81%E4%B8%AA%E7%90%86%E7%94%B1%E5%91%8A%E8%AF%89%E4%BD%A0%E6%88%91%E4%BB%AC%E9%94%99%E7%9C%8B%E4%BA%86%E4%B8%96%E7%95%8C.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%9C%9F%E7%9B%B8%EF%BC%9A%E5%8D%81%E4%B8%AA%E7%90%86%E7%94%B1%E5%91%8A%E8%AF%89%E4%BD%A0%E6%88%91%E4%BB%AC%E9%94%99%E7%9C%8B%E4%BA%86%E4%B8%96%E7%95%8C.epub)
* 大脑整理术 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A7%E8%84%91%E6%95%B4%E7%90%86%E6%9C%AF.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A7%E8%84%91%E6%95%B4%E7%90%86%E6%9C%AF.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A7%E8%84%91%E6%95%B4%E7%90%86%E6%9C%AF.azw3)
* 从祖先到算法 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E7%A5%96%E5%85%88%E5%88%B0%E7%AE%97%E6%B3%95.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E7%A5%96%E5%85%88%E5%88%B0%E7%AE%97%E6%B3%95.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E7%A5%96%E5%85%88%E5%88%B0%E7%AE%97%E6%B3%95.azw3)
* 电竞简史 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%94%B5%E7%AB%9E%E7%AE%80%E5%8F%B2.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%94%B5%E7%AB%9E%E7%AE%80%E5%8F%B2.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%94%B5%E7%AB%9E%E7%AE%80%E5%8F%B2.azw3)
* 柏拉图与技术呆子：人类与技术的创造性伙伴关系 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9F%8F%E6%8B%89%E5%9B%BE%E4%B8%8E%E6%8A%80%E6%9C%AF%E5%91%86%E5%AD%90%EF%BC%9A%E4%BA%BA%E7%B1%BB%E4%B8%8E%E6%8A%80%E6%9C%AF%E7%9A%84%E5%88%9B%E9%80%A0%E6%80%A7%E4%BC%99%E4%BC%B4%E5%85%B3%E7%B3%BB.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%9F%8F%E6%8B%89%E5%9B%BE%E4%B8%8E%E6%8A%80%E6%9C%AF%E5%91%86%E5%AD%90%EF%BC%9A%E4%BA%BA%E7%B1%BB%E4%B8%8E%E6%8A%80%E6%9C%AF%E7%9A%84%E5%88%9B%E9%80%A0%E6%80%A7%E4%BC%99%E4%BC%B4%E5%85%B3%E7%B3%BB.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%9F%8F%E6%8B%89%E5%9B%BE%E4%B8%8E%E6%8A%80%E6%9C%AF%E5%91%86%E5%AD%90%EF%BC%9A%E4%BA%BA%E7%B1%BB%E4%B8%8E%E6%8A%80%E6%9C%AF%E7%9A%84%E5%88%9B%E9%80%A0%E6%80%A7%E4%BC%99%E4%BC%B4%E5%85%B3%E7%B3%BB.azw3)
*  万物皆假设 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%87%E7%89%A9%E7%9A%86%E5%81%87%E8%AE%BE.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%87%E7%89%A9%E7%9A%86%E5%81%87%E8%AE%BE.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%87%E7%89%A9%E7%9A%86%E5%81%87%E8%AE%BE.azw3)
* What if 那些古怪又让人忧心的问题 [mobi](https://oss-blog.cdn.hujingnb.com/book/What%20if%20%E9%82%A3%E4%BA%9B%E5%8F%A4%E6%80%AA%E5%8F%88%E8%AE%A9%E4%BA%BA%E5%BF%A7%E5%BF%83%E7%9A%84%E9%97%AE%E9%A2%98.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/What%20if%20%E9%82%A3%E4%BA%9B%E5%8F%A4%E6%80%AA%E5%8F%88%E8%AE%A9%E4%BA%BA%E5%BF%A7%E5%BF%83%E7%9A%84%E9%97%AE%E9%A2%98.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/What%20if%20%E9%82%A3%E4%BA%9B%E5%8F%A4%E6%80%AA%E5%8F%88%E8%AE%A9%E4%BA%BA%E5%BF%A7%E5%BF%83%E7%9A%84%E9%97%AE%E9%A2%98.azw3)
* How to：如何不切实际地解决实际问题 [mobi](https://oss-blog.cdn.hujingnb.com/book/How%20to%EF%BC%9A%E5%A6%82%E4%BD%95%E4%B8%8D%E5%88%87%E5%AE%9E%E9%99%85%E5%9C%B0%E8%A7%A3%E5%86%B3%E5%AE%9E%E9%99%85%E9%97%AE%E9%A2%98.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/How%20to%EF%BC%9A%E5%A6%82%E4%BD%95%E4%B8%8D%E5%88%87%E5%AE%9E%E9%99%85%E5%9C%B0%E8%A7%A3%E5%86%B3%E5%AE%9E%E9%99%85%E9%97%AE%E9%A2%98.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/How%20to%EF%BC%9A%E5%A6%82%E4%BD%95%E4%B8%8D%E5%88%87%E5%AE%9E%E9%99%85%E5%9C%B0%E8%A7%A3%E5%86%B3%E5%AE%9E%E9%99%85%E9%97%AE%E9%A2%98.azw3)


# 表达

## 沟通

* 关键对话如何高效能沟通原书第2版 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%85%B3%E9%94%AE%E5%AF%B9%E8%AF%9D%E5%A6%82%E4%BD%95%E9%AB%98%E6%95%88%E8%83%BD%E6%B2%9F%E9%80%9A%E5%8E%9F%E4%B9%A6%E7%AC%AC2%E7%89%88.mobi)
* 学会提问 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%AD%A6%E4%BC%9A%E6%8F%90%E9%97%AE.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%AD%A6%E4%BC%9A%E6%8F%90%E9%97%AE.epub)
* 遇谁都能聊得开 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%81%87%E8%B0%81%E9%83%BD%E8%83%BD%E8%81%8A%E5%BE%97%E5%BC%80.mobi)
* 别独自用餐克林顿还是穷小子时如何建立顶级社交圈十周年作者修订珍藏版 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%88%AB%E7%8B%AC%E8%87%AA%E7%94%A8%E9%A4%90%E5%85%8B%E6%9E%97%E9%A1%BF%E8%BF%98%E6%98%AF%E7%A9%B7%E5%B0%8F%E5%AD%90%E6%97%B6%E5%A6%82%E4%BD%95%E5%BB%BA%E7%AB%8B%E9%A1%B6%E7%BA%A7%E7%A4%BE%E4%BA%A4%E5%9C%88%E5%8D%81%E5%91%A8%E5%B9%B4%E4%BD%9C%E8%80%85%E4%BF%AE%E8%AE%A2%E7%8F%8D%E8%97%8F%E7%89%88.mobi)
* 沟通听说读写全方位沟通技巧从沟通开始成就一个社会人的自我 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B2%9F%E9%80%9A%E5%90%AC%E8%AF%B4%E8%AF%BB%E5%86%99%E5%85%A8%E6%96%B9%E4%BD%8D%E6%B2%9F%E9%80%9A%E6%8A%80%E5%B7%A7%E4%BB%8E%E6%B2%9F%E9%80%9A%E5%BC%80%E5%A7%8B%E6%88%90%E5%B0%B1%E4%B8%80%E4%B8%AA%E7%A4%BE%E4%BC%9A%E4%BA%BA%E7%9A%84%E8%87%AA%E6%88%91.mobi)
* 幽默沟通学零距离制胜的口才秘籍 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%B9%BD%E9%BB%98%E6%B2%9F%E9%80%9A%E5%AD%A6%E9%9B%B6%E8%B7%9D%E7%A6%BB%E5%88%B6%E8%83%9C%E7%9A%84%E5%8F%A3%E6%89%8D%E7%A7%98%E7%B1%8D.mobi)
* 影响力经典版 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%B1%E5%93%8D%E5%8A%9B%E7%BB%8F%E5%85%B8%E7%89%88.mobi)
* 中国式场面话大全-杨百平 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%AD%E5%9B%BD%E5%BC%8F%E5%9C%BA%E9%9D%A2%E8%AF%9D%E5%A4%A7%E5%85%A8-%E6%9D%A8%E7%99%BE%E5%B9%B3.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%AD%E5%9B%BD%E5%BC%8F%E5%9C%BA%E9%9D%A2%E8%AF%9D%E5%A4%A7%E5%85%A8-%E6%9D%A8%E7%99%BE%E5%B9%B3.epub) 
* 轻松说服 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%BD%BB%E6%9D%BE%E8%AF%B4%E6%9C%8D.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%BD%BB%E6%9D%BE%E8%AF%B4%E6%9C%8D.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E8%BD%BB%E6%9D%BE%E8%AF%B4%E6%9C%8D.azw3)
* 把话说到点子上 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%8A%8A%E8%AF%9D%E8%AF%B4%E5%88%B0%E7%82%B9%E5%AD%90%E4%B8%8A.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%8A%8A%E8%AF%9D%E8%AF%B4%E5%88%B0%E7%82%B9%E5%AD%90%E4%B8%8A.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%8A%8A%E8%AF%9D%E8%AF%B4%E5%88%B0%E7%82%B9%E5%AD%90%E4%B8%8A.azw3)
* 当时这样说就好了 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%93%E6%97%B6%E8%BF%99%E6%A0%B7%E8%AF%B4%E5%B0%B1%E5%A5%BD%E4%BA%86.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%93%E6%97%B6%E8%BF%99%E6%A0%B7%E8%AF%B4%E5%B0%B1%E5%A5%BD%E4%BA%86.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%93%E6%97%B6%E8%BF%99%E6%A0%B7%E8%AF%B4%E5%B0%B1%E5%A5%BD%E4%BA%86.azw3)
* 说笑：有效有范儿的表达技巧 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%AF%B4%E7%AC%91%EF%BC%9A%E6%9C%89%E6%95%88%E6%9C%89%E8%8C%83%E5%84%BF%E7%9A%84%E8%A1%A8%E8%BE%BE%E6%8A%80%E5%B7%A7.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%AF%B4%E7%AC%91%EF%BC%9A%E6%9C%89%E6%95%88%E6%9C%89%E8%8C%83%E5%84%BF%E7%9A%84%E8%A1%A8%E8%BE%BE%E6%8A%80%E5%B7%A7.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E8%AF%B4%E7%AC%91%EF%BC%9A%E6%9C%89%E6%95%88%E6%9C%89%E8%8C%83%E5%84%BF%E7%9A%84%E8%A1%A8%E8%BE%BE%E6%8A%80%E5%B7%A7.azw3)
* 非暴力沟通 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%9D%9E%E6%9A%B4%E5%8A%9B%E6%B2%9F%E9%80%9A.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E9%9D%9E%E6%9A%B4%E5%8A%9B%E6%B2%9F%E9%80%9A.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E9%9D%9E%E6%9A%B4%E5%8A%9B%E6%B2%9F%E9%80%9A.azw3)
* 好好拜託 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A5%BD%E5%A5%BD%E6%8B%9C%E8%A8%97.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A5%BD%E5%A5%BD%E6%8B%9C%E8%A8%97.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A5%BD%E5%A5%BD%E6%8B%9C%E8%A8%97.azw3)
* 如何搞定难相处的人 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E6%90%9E%E5%AE%9A%E9%9A%BE%E7%9B%B8%E5%A4%84%E7%9A%84%E4%BA%BA.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E6%90%9E%E5%AE%9A%E9%9A%BE%E7%9B%B8%E5%A4%84%E7%9A%84%E4%BA%BA.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E6%90%9E%E5%AE%9A%E9%9A%BE%E7%9B%B8%E5%A4%84%E7%9A%84%E4%BA%BA.azw3)

## 演讲

* 乔布斯的魔力演讲精编图文版  [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B9%94%E5%B8%83%E6%96%AF%E7%9A%84%E9%AD%94%E5%8A%9B%E6%BC%94%E8%AE%B2%E7%B2%BE%E7%BC%96%E5%9B%BE%E6%96%87%E7%89%88.mobi)
* 即兴表达 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%8D%B3%E5%85%B4%E8%A1%A8%E8%BE%BE.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%8D%B3%E5%85%B4%E8%A1%A8%E8%BE%BE.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%8D%B3%E5%85%B4%E8%A1%A8%E8%BE%BE.azw3)
* 一分钟口才训练 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%80%E5%88%86%E9%92%9F%E5%8F%A3%E6%89%8D%E8%AE%AD%E7%BB%83.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%80%E5%88%86%E9%92%9F%E5%8F%A3%E6%89%8D%E8%AE%AD%E7%BB%83.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%80%E5%88%86%E9%92%9F%E5%8F%A3%E6%89%8D%E8%AE%AD%E7%BB%83.azw3)
* 从不会说话到演讲高手 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E4%B8%8D%E4%BC%9A%E8%AF%B4%E8%AF%9D%E5%88%B0%E6%BC%94%E8%AE%B2%E9%AB%98%E6%89%8B.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E4%B8%8D%E4%BC%9A%E8%AF%B4%E8%AF%9D%E5%88%B0%E6%BC%94%E8%AE%B2%E9%AB%98%E6%89%8B.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BB%8E%E4%B8%8D%E4%BC%9A%E8%AF%B4%E8%AF%9D%E5%88%B0%E6%BC%94%E8%AE%B2%E9%AB%98%E6%89%8B.azw3)

## 脱口秀

* 如何提升段子的“笑”果 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E6%8F%90%E5%8D%87%E6%AE%B5%E5%AD%90%E7%9A%84%E2%80%9C%E7%AC%91%E2%80%9D%E6%9E%9C.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E6%8F%90%E5%8D%87%E6%AE%B5%E5%AD%90%E7%9A%84%E2%80%9C%E7%AC%91%E2%80%9D%E6%9E%9C.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E6%8F%90%E5%8D%87%E6%AE%B5%E5%AD%90%E7%9A%84%E2%80%9C%E7%AC%91%E2%80%9D%E6%9E%9C.azw3)
* 如何成为一名脱口秀老手 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E6%88%90%E4%B8%BA%E4%B8%80%E5%90%8D%E8%84%B1%E5%8F%A3%E7%A7%80%E8%80%81%E6%89%8B.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E6%88%90%E4%B8%BA%E4%B8%80%E5%90%8D%E8%84%B1%E5%8F%A3%E7%A7%80%E8%80%81%E6%89%8B.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E4%BD%95%E6%88%90%E4%B8%BA%E4%B8%80%E5%90%8D%E8%84%B1%E5%8F%A3%E7%A7%80%E8%80%81%E6%89%8B.azw3)
* 手把手教你玩脱口秀 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%89%8B%E6%8A%8A%E6%89%8B%E6%95%99%E4%BD%A0%E7%8E%A9%E8%84%B1%E5%8F%A3%E7%A7%80.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%89%8B%E6%8A%8A%E6%89%8B%E6%95%99%E4%BD%A0%E7%8E%A9%E8%84%B1%E5%8F%A3%E7%A7%80.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%89%8B%E6%8A%8A%E6%89%8B%E6%95%99%E4%BD%A0%E7%8E%A9%E8%84%B1%E5%8F%A3%E7%A7%80.azw3)

## 写作

* 10倍写作术 [mobi](https://oss-blog.cdn.hujingnb.com/book/10%E5%80%8D%E5%86%99%E4%BD%9C%E6%9C%AF.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/10%E5%80%8D%E5%86%99%E4%BD%9C%E6%9C%AF.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/10%E5%80%8D%E5%86%99%E4%BD%9C%E6%9C%AF.azw3)
* 爆款写作 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%88%86%E6%AC%BE%E5%86%99%E4%BD%9C.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%88%86%E6%AC%BE%E5%86%99%E4%BD%9C.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%88%86%E6%AC%BE%E5%86%99%E4%BD%9C.azw3)
* 学会写作：成为真正会表达的人 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%AD%A6%E4%BC%9A%E5%86%99%E4%BD%9C%EF%BC%9A%E6%88%90%E4%B8%BA%E7%9C%9F%E6%AD%A3%E4%BC%9A%E8%A1%A8%E8%BE%BE%E7%9A%84%E4%BA%BA.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%AD%A6%E4%BC%9A%E5%86%99%E4%BD%9C%EF%BC%9A%E6%88%90%E4%B8%BA%E7%9C%9F%E6%AD%A3%E4%BC%9A%E8%A1%A8%E8%BE%BE%E7%9A%84%E4%BA%BA.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%AD%A6%E4%BC%9A%E5%86%99%E4%BD%9C%EF%BC%9A%E6%88%90%E4%B8%BA%E7%9C%9F%E6%AD%A3%E4%BC%9A%E8%A1%A8%E8%BE%BE%E7%9A%84%E4%BA%BA.azw3)
* 完全写作指南 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%AE%8C%E5%85%A8%E5%86%99%E4%BD%9C%E6%8C%87%E5%8D%97.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%AE%8C%E5%85%A8%E5%86%99%E4%BD%9C%E6%8C%87%E5%8D%97.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%AE%8C%E5%85%A8%E5%86%99%E4%BD%9C%E6%8C%87%E5%8D%97.azw3)
* 马上写出好文章 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E9%A9%AC%E4%B8%8A%E5%86%99%E5%87%BA%E5%A5%BD%E6%96%87%E7%AB%A0.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E9%A9%AC%E4%B8%8A%E5%86%99%E5%87%BA%E5%A5%BD%E6%96%87%E7%AB%A0.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E9%A9%AC%E4%B8%8A%E5%86%99%E5%87%BA%E5%A5%BD%E6%96%87%E7%AB%A0.azw3)


# 思考

* 一个人的朝圣 [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%80%E4%B8%AA%E4%BA%BA%E7%9A%84%E6%9C%9D%E5%9C%A3.azw3)
* 天才在左疯子在右 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%A9%E6%89%8D%E5%9C%A8%E5%B7%A6%E7%96%AF%E5%AD%90%E5%9C%A8%E5%8F%B3.mobi)
* 超越智商：为什么聪明人也会做蠢事 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E8%B6%8A%E6%99%BA%E5%95%86%EF%BC%9A%E4%B8%BA%E4%BB%80%E4%B9%88%E8%81%AA%E6%98%8E%E4%BA%BA%E4%B9%9F%E4%BC%9A%E5%81%9A%E8%A0%A2%E4%BA%8B.mobi)
* 哲学家们都干了些什么 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%93%B2%E5%AD%A6%E5%AE%B6%E4%BB%AC%E9%83%BD%E5%B9%B2%E4%BA%86%E4%BA%9B%E4%BB%80%E4%B9%88.mobi)
* 哲学的故事让深奥的哲学立刻生动起来 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%93%B2%E5%AD%A6%E7%9A%84%E6%95%85%E4%BA%8B%E8%AE%A9%E6%B7%B1%E5%A5%A5%E7%9A%84%E5%93%B2%E5%AD%A6%E7%AB%8B%E5%88%BB%E7%94%9F%E5%8A%A8%E8%B5%B7%E6%9D%A5.mobi)
* 如果没有今天明天会不会有昨天 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A6%82%E6%9E%9C%E6%B2%A1%E6%9C%89%E4%BB%8A%E5%A4%A9%E6%98%8E%E5%A4%A9%E4%BC%9A%E4%B8%8D%E4%BC%9A%E6%9C%89%E6%98%A8%E5%A4%A9.mobi)
* 去依附 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%8E%BB%E4%BE%9D%E9%99%84.mobi)
* 同步：秩序如何从混沌中涌现 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%90%8C%E6%AD%A5%EF%BC%9A%E7%A7%A9%E5%BA%8F%E5%A6%82%E4%BD%95%E4%BB%8E%E6%B7%B7%E6%B2%8C%E4%B8%AD%E6%B6%8C%E7%8E%B0.mobi)
* 异类不一样的成功启示录 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%BC%82%E7%B1%BB%E4%B8%8D%E4%B8%80%E6%A0%B7%E7%9A%84%E6%88%90%E5%8A%9F%E5%90%AF%E7%A4%BA%E5%BD%95.mobi)
* 推理的迷宫：悖论、谜题及知识的脆弱性 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%8E%A8%E7%90%86%E7%9A%84%E8%BF%B7%E5%AE%AB%EF%BC%9A%E6%82%96%E8%AE%BA%E3%80%81%E8%B0%9C%E9%A2%98%E5%8F%8A%E7%9F%A5%E8%AF%86%E7%9A%84%E8%84%86%E5%BC%B1%E6%80%A7.mobi)
* 思辨与立场生活中无处不在的批判性思维工具 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%80%9D%E8%BE%A8%E4%B8%8E%E7%AB%8B%E5%9C%BA%E7%94%9F%E6%B4%BB%E4%B8%AD%E6%97%A0%E5%A4%84%E4%B8%8D%E5%9C%A8%E7%9A%84%E6%89%B9%E5%88%A4%E6%80%A7%E6%80%9D%E7%BB%B4%E5%B7%A5%E5%85%B7.mobi)
* 简单逻辑学改变思维方式第一书 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%AE%80%E5%8D%95%E9%80%BB%E8%BE%91%E5%AD%A6%E6%94%B9%E5%8F%98%E6%80%9D%E7%BB%B4%E6%96%B9%E5%BC%8F%E7%AC%AC%E4%B8%80%E4%B9%A6.mobi)
* 真名实姓 [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%9C%9F%E5%90%8D%E5%AE%9E%E5%A7%93.azw3) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%9C%9F%E5%90%8D%E5%AE%9E%E5%A7%93.mobi)
* 深夜加油站遇见苏格拉底 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B7%B1%E5%A4%9C%E5%8A%A0%E6%B2%B9%E7%AB%99%E9%81%87%E8%A7%81%E8%8B%8F%E6%A0%BC%E6%8B%89%E5%BA%95.mobi)
* 我脑海里住着一个自我怀疑又自作聪明的人未读思想家 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E8%84%91%E6%B5%B7%E9%87%8C%E4%BD%8F%E7%9D%80%E4%B8%80%E4%B8%AA%E8%87%AA%E6%88%91%E6%80%80%E7%96%91%E5%8F%88%E8%87%AA%E4%BD%9C%E8%81%AA%E6%98%8E%E7%9A%84%E4%BA%BA%E6%9C%AA%E8%AF%BB%E6%80%9D%E6%83%B3%E5%AE%B6.mobi)
* 断舍离 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%96%AD%E8%88%8D%E7%A6%BB.mobi)
* 查拉图斯特拉如是说尼采经典著作 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9F%A5%E6%8B%89%E5%9B%BE%E6%96%AF%E7%89%B9%E6%8B%89%E5%A6%82%E6%98%AF%E8%AF%B4%E5%B0%BC%E9%87%87%E7%BB%8F%E5%85%B8%E8%91%97%E4%BD%9C.mobi)
* 人为什么活着 [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E4%B8%BA%E4%BB%80%E4%B9%88%E6%B4%BB%E7%9D%80.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E4%B8%BA%E4%BB%80%E4%B9%88%E6%B4%BB%E7%9D%80.mobi)
* 你的灯亮着吗发现问题的真正所在 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BD%A0%E7%9A%84%E7%81%AF%E4%BA%AE%E7%9D%80%E5%90%97%E5%8F%91%E7%8E%B0%E9%97%AE%E9%A2%98%E7%9A%84%E7%9C%9F%E6%AD%A3%E6%89%80%E5%9C%A8.mobi)
* 清醒思考的艺术 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B8%85%E9%86%92%E6%80%9D%E8%80%83%E7%9A%84%E8%89%BA%E6%9C%AF.mobi)
* 禅与摩托车维修艺术 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%A6%85%E4%B8%8E%E6%91%A9%E6%89%98%E8%BD%A6%E7%BB%B4%E4%BF%AE%E8%89%BA%E6%9C%AF.mobi)
* 给理想一点时间 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%99%E7%90%86%E6%83%B3%E4%B8%80%E7%82%B9%E6%97%B6%E9%97%B4.mobi)
* 聪明人的才华战略 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%81%AA%E6%98%8E%E4%BA%BA%E7%9A%84%E6%89%8D%E5%8D%8E%E6%88%98%E7%95%A5.mobi)
* 苏格拉底之死（译文经典） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%8B%8F%E6%A0%BC%E6%8B%89%E5%BA%95%E4%B9%8B%E6%AD%BB%EF%BC%88%E8%AF%91%E6%96%87%E7%BB%8F%E5%85%B8%EF%BC%89.mobi)
* 被讨厌的勇气 [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%A2%AB%E8%AE%A8%E5%8E%8C%E7%9A%84%E5%8B%87%E6%B0%94.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%A2%AB%E8%AE%A8%E5%8E%8C%E7%9A%84%E5%8B%87%E6%B0%94.mobi)
* 诡辩与真相 [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%AF%A1%E8%BE%A9%E4%B8%8E%E7%9C%9F%E7%9B%B8.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%AF%A1%E8%BE%A9%E4%B8%8E%E7%9C%9F%E7%9B%B8.mobi)
* 读书毁了我 [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%AF%BB%E4%B9%A6%E6%AF%81%E4%BA%86%E6%88%91.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%AF%BB%E4%B9%A6%E6%AF%81%E4%BA%86%E6%88%91.mobi)
* 有限与无限的游戏-一个哲学家眼中的竞技世界 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9C%89%E9%99%90%E4%B8%8E%E6%97%A0%E9%99%90%E7%9A%84%E6%B8%B8%E6%88%8F-%E4%B8%80%E4%B8%AA%E5%93%B2%E5%AD%A6%E5%AE%B6%E7%9C%BC%E4%B8%AD%E7%9A%84%E7%AB%9E%E6%8A%80%E4%B8%96%E7%95%8C.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%9C%89%E9%99%90%E4%B8%8E%E6%97%A0%E9%99%90%E7%9A%84%E6%B8%B8%E6%88%8F-%E4%B8%80%E4%B8%AA%E5%93%B2%E5%AD%A6%E5%AE%B6%E7%9C%BC%E4%B8%AD%E7%9A%84%E7%AB%9E%E6%8A%80%E4%B8%96%E7%95%8C.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%9C%89%E9%99%90%E4%B8%8E%E6%97%A0%E9%99%90%E7%9A%84%E6%B8%B8%E6%88%8F-%E4%B8%80%E4%B8%AA%E5%93%B2%E5%AD%A6%E5%AE%B6%E7%9C%BC%E4%B8%AD%E7%9A%84%E7%AB%9E%E6%8A%80%E4%B8%96%E7%95%8C.azw3)
* 为什么：关于因果关系的新科学 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%BA%E4%BB%80%E4%B9%88%EF%BC%9A%E5%85%B3%E4%BA%8E%E5%9B%A0%E6%9E%9C%E5%85%B3%E7%B3%BB%E7%9A%84%E6%96%B0%E7%A7%91%E5%AD%A6.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%BA%E4%BB%80%E4%B9%88%EF%BC%9A%E5%85%B3%E4%BA%8E%E5%9B%A0%E6%9E%9C%E5%85%B3%E7%B3%BB%E7%9A%84%E6%96%B0%E7%A7%91%E5%AD%A6.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%BA%E4%BB%80%E4%B9%88%EF%BC%9A%E5%85%B3%E4%BA%8E%E5%9B%A0%E6%9E%9C%E5%85%B3%E7%B3%BB%E7%9A%84%E6%96%B0%E7%A7%91%E5%AD%A6.azw3)
* 拆掉思维里的墙 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%8B%86%E6%8E%89%E6%80%9D%E7%BB%B4%E9%87%8C%E7%9A%84%E5%A2%99.mobi) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%8B%86%E6%8E%89%E6%80%9D%E7%BB%B4%E9%87%8C%E7%9A%84%E5%A2%99.azw3)
* 把时间当作朋友（第3版） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%8A%8A%E6%97%B6%E9%97%B4%E5%BD%93%E4%BD%9C%E6%9C%8B%E5%8F%8B%EF%BC%88%E7%AC%AC3%E7%89%88%EF%BC%89.mobi)
* 思维盲点：任何人都会有的思维误区 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%80%9D%E7%BB%B4%E7%9B%B2%E7%82%B9%EF%BC%9A%E4%BB%BB%E4%BD%95%E4%BA%BA%E9%83%BD%E4%BC%9A%E6%9C%89%E7%9A%84%E6%80%9D%E7%BB%B4%E8%AF%AF%E5%8C%BA.mobi) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%80%9D%E7%BB%B4%E7%9B%B2%E7%82%B9%EF%BC%9A%E4%BB%BB%E4%BD%95%E4%BA%BA%E9%83%BD%E4%BC%9A%E6%9C%89%E7%9A%84%E6%80%9D%E7%BB%B4%E8%AF%AF%E5%8C%BA.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%80%9D%E7%BB%B4%E7%9B%B2%E7%82%B9%EF%BC%9A%E4%BB%BB%E4%BD%95%E4%BA%BA%E9%83%BD%E4%BC%9A%E6%9C%89%E7%9A%84%E6%80%9D%E7%BB%B4%E8%AF%AF%E5%8C%BA.azw3)
* 人生五大问题 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E7%94%9F%E4%BA%94%E5%A4%A7%E9%97%AE%E9%A2%98.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E7%94%9F%E4%BA%94%E5%A4%A7%E9%97%AE%E9%A2%98.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E7%94%9F%E4%BA%94%E5%A4%A7%E9%97%AE%E9%A2%98.azw3)



# 读物

## 著作

* 月亮和六便士 [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%9C%88%E4%BA%AE%E5%92%8C%E5%85%AD%E4%BE%BF%E5%A3%AB.azw3)
* 活着-余华 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B4%BB%E7%9D%80-%E4%BD%99%E5%8D%8E.mobi)
* 脑髓地狱日本四大推理奇书 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%84%91%E9%AB%93%E5%9C%B0%E7%8B%B1%E6%97%A5%E6%9C%AC%E5%9B%9B%E5%A4%A7%E6%8E%A8%E7%90%86%E5%A5%87%E4%B9%A6.mobi)
* 无人生还 [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%97%A0%E4%BA%BA%E7%94%9F%E8%BF%98.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%97%A0%E4%BA%BA%E7%94%9F%E8%BF%98.mobi)
* 超脑（套装共2册） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E8%84%91%EF%BC%88%E5%A5%97%E8%A3%85%E5%85%B12%E5%86%8C%EF%BC%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E8%84%91%EF%BC%88%E5%A5%97%E8%A3%85%E5%85%B12%E5%86%8C%EF%BC%89.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E8%84%91%EF%BC%88%E5%A5%97%E8%A3%85%E5%85%B12%E5%86%8C%EF%BC%89.azw3)
* 人性中的善良天使 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E6%80%A7%E4%B8%AD%E7%9A%84%E5%96%84%E8%89%AF%E5%A4%A9%E4%BD%BF.mobi)
* 当呼吸化为空气 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%93%E5%91%BC%E5%90%B8%E5%8C%96%E4%B8%BA%E7%A9%BA%E6%B0%94.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%93%E5%91%BC%E5%90%B8%E5%8C%96%E4%B8%BA%E7%A9%BA%E6%B0%94.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%93%E5%91%BC%E5%90%B8%E5%8C%96%E4%B8%BA%E7%A9%BA%E6%B0%94.azw3)
* 当下的启蒙：为理性、科学、人文主义和进步辩护 [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%93%E4%B8%8B%E7%9A%84%E5%90%AF%E8%92%99%EF%BC%9A%E4%B8%BA%E7%90%86%E6%80%A7%E3%80%81%E7%A7%91%E5%AD%A6%E3%80%81%E4%BA%BA%E6%96%87%E4%B8%BB%E4%B9%89%E5%92%8C%E8%BF%9B%E6%AD%A5%E8%BE%A9%E6%8A%A4.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%93%E4%B8%8B%E7%9A%84%E5%90%AF%E8%92%99%EF%BC%9A%E4%B8%BA%E7%90%86%E6%80%A7%E3%80%81%E7%A7%91%E5%AD%A6%E3%80%81%E4%BA%BA%E6%96%87%E4%B8%BB%E4%B9%89%E5%92%8C%E8%BF%9B%E6%AD%A5%E8%BE%A9%E6%8A%A4.azw3) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%93%E4%B8%8B%E7%9A%84%E5%90%AF%E8%92%99%EF%BC%9A%E4%B8%BA%E7%90%86%E6%80%A7%E3%80%81%E7%A7%91%E5%AD%A6%E3%80%81%E4%BA%BA%E6%96%87%E4%B8%BB%E4%B9%89%E5%92%8C%E8%BF%9B%E6%AD%A5%E8%BE%A9%E6%8A%A4.mobi) [pdf](https://oss-blog.cdn.hujingnb.com/book/%E5%BD%93%E4%B8%8B%E7%9A%84%E5%90%AF%E8%92%99%EF%BC%9A%E4%B8%BA%E7%90%86%E6%80%A7%E3%80%81%E7%A7%91%E5%AD%A6%E3%80%81%E4%BA%BA%E6%96%87%E4%B8%BB%E4%B9%89%E5%92%8C%E8%BF%9B%E6%AD%A5%E8%BE%A9%E6%8A%A4.pdf)
* 三体全集(精校注释版) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%89%E4%BD%93%E5%85%A8%E9%9B%86(%E7%B2%BE%E6%A0%A1%E6%B3%A8%E9%87%8A%E7%89%88).mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%89%E4%BD%93%E5%85%A8%E9%9B%86(%E7%B2%BE%E6%A0%A1%E6%B3%A8%E9%87%8A%E7%89%88).epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%89%E4%BD%93%E5%85%A8%E9%9B%86(%E7%B2%BE%E6%A0%A1%E6%B3%A8%E9%87%8A%E7%89%88).azw3)
* 基度山伯爵（全2册） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%9F%BA%E5%BA%A6%E5%B1%B1%E4%BC%AF%E7%88%B5%EF%BC%88%E5%85%A82%E5%86%8C%EF%BC%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%9F%BA%E5%BA%A6%E5%B1%B1%E4%BC%AF%E7%88%B5%EF%BC%88%E5%85%A82%E5%86%8C%EF%BC%89.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%9F%BA%E5%BA%A6%E5%B1%B1%E4%BC%AF%E7%88%B5%EF%BC%88%E5%85%A82%E5%86%8C%EF%BC%89.azw3)
* 少年维特的烦恼 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%B0%91%E5%B9%B4%E7%BB%B4%E7%89%B9%E7%9A%84%E7%83%A6%E6%81%BC.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%B0%91%E5%B9%B4%E7%BB%B4%E7%89%B9%E7%9A%84%E7%83%A6%E6%81%BC.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%B0%91%E5%B9%B4%E7%BB%B4%E7%89%B9%E7%9A%84%E7%83%A6%E6%81%BC.azw3)
* 伊索寓言 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BC%8A%E7%B4%A2%E5%AF%93%E8%A8%80.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BC%8A%E7%B4%A2%E5%AF%93%E8%A8%80.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BC%8A%E7%B4%A2%E5%AF%93%E8%A8%80.azw3)
* 我弥留之际 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E5%BC%A5%E7%95%99%E4%B9%8B%E9%99%85.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E5%BC%A5%E7%95%99%E4%B9%8B%E9%99%85.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%88%91%E5%BC%A5%E7%95%99%E4%B9%8B%E9%99%85.azw3)
* 失落世界（套装2册） [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%B1%E8%90%BD%E4%B8%96%E7%95%8C%EF%BC%88%E5%A5%97%E8%A3%852%E5%86%8C%EF%BC%89.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%B1%E8%90%BD%E4%B8%96%E7%95%8C%EF%BC%88%E5%A5%97%E8%A3%852%E5%86%8C%EF%BC%89.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A4%B1%E8%90%BD%E4%B8%96%E7%95%8C%EF%BC%88%E5%A5%97%E8%A3%852%E5%86%8C%EF%BC%89.azw3)
* 变形记 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%8F%98%E5%BD%A2%E8%AE%B0.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%8F%98%E5%BD%A2%E8%AE%B0.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%8F%98%E5%BD%A2%E8%AE%B0.azw3)
* 月亮与六便士 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9C%88%E4%BA%AE%E4%B8%8E%E5%85%AD%E4%BE%BF%E5%A3%AB.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%9C%88%E4%BA%AE%E4%B8%8E%E5%85%AD%E4%BE%BF%E5%A3%AB.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%9C%88%E4%BA%AE%E4%B8%8E%E5%85%AD%E4%BE%BF%E5%A3%AB.azw3)

## 小说

* 只因暮色难寻 [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%8F%AA%E5%9B%A0%E6%9A%AE%E8%89%B2%E9%9A%BE%E5%AF%BB.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%8F%AA%E5%9B%A0%E6%9A%AE%E8%89%B2%E9%9A%BE%E5%AF%BB.azw3)


## 其他

* 《人类的群星闪耀时（增订版）》作者-斯蒂芬·茨威格 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E3%80%8A%E4%BA%BA%E7%B1%BB%E7%9A%84%E7%BE%A4%E6%98%9F%E9%97%AA%E8%80%80%E6%97%B6%EF%BC%88%E5%A2%9E%E8%AE%A2%E7%89%88%EF%BC%89%E3%80%8B%E4%BD%9C%E8%80%85_-%5B%E5%A5%A5%5D-%E6%96%AF%E8%92%82%E8%8A%AC%C2%B7%E8%8C%A8%E5%A8%81%E6%A0%BC.mobi)
* 俗世奇人 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BF%97%E4%B8%96%E5%A5%87%E4%BA%BA.mobi)
* 给青年的十二封信 [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%99%E9%9D%92%E5%B9%B4%E7%9A%84%E5%8D%81%E4%BA%8C%E5%B0%81%E4%BF%A1.epub) [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%99%E9%9D%92%E5%B9%B4%E7%9A%84%E5%8D%81%E4%BA%8C%E5%B0%81%E4%BF%A1.mobi)
* 走近费曼丛书别逗了费曼先生科学鬼才费曼的逗逼人生 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%B5%B0%E8%BF%91%E8%B4%B9%E6%9B%BC%E4%B8%9B%E4%B9%A6%E5%88%AB%E9%80%97%E4%BA%86%E8%B4%B9%E6%9B%BC%E5%85%88%E7%94%9F%E7%A7%91%E5%AD%A6%E9%AC%BC%E6%89%8D%E8%B4%B9%E6%9B%BC%E7%9A%84%E9%80%97%E9%80%BC%E4%BA%BA%E7%94%9F.mobi)
* 别闹了费曼先生 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%88%AB%E9%97%B9%E4%BA%86%E8%B4%B9%E6%9B%BC%E5%85%88%E7%94%9F.mobi)
* 过得刚好郭德纲亲笔作品讲述人生四十多年的江湖过往 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%BF%87%E5%BE%97%E5%88%9A%E5%A5%BD%E9%83%AD%E5%BE%B7%E7%BA%B2%E4%BA%B2%E7%AC%94%E4%BD%9C%E5%93%81%E8%AE%B2%E8%BF%B0%E4%BA%BA%E7%94%9F%E5%9B%9B%E5%8D%81%E5%A4%9A%E5%B9%B4%E7%9A%84%E6%B1%9F%E6%B9%96%E8%BF%87%E5%BE%80.mobi)
* 极客爱情 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%9E%81%E5%AE%A2%E7%88%B1%E6%83%85.mobi)
* 程序江湖 [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%A8%8B%E5%BA%8F%E6%B1%9F%E6%B9%96.epub)   
* 笑场 李诞 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%AC%91%E5%9C%BA%20%E6%9D%8E%E8%AF%9E.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%AC%91%E5%9C%BA%20%E6%9D%8E%E8%AF%9E.epub)
* 与恶魔对话 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%8E%E6%81%B6%E9%AD%94%E5%AF%B9%E8%AF%9D.mobi)
* 世界未解之谜大全集超值白金版超级彩图馆 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%B8%96%E7%95%8C%E6%9C%AA%E8%A7%A3%E4%B9%8B%E8%B0%9C%E5%A4%A7%E5%85%A8%E9%9B%86%E8%B6%85%E5%80%BC%E7%99%BD%E9%87%91%E7%89%88%E8%B6%85%E7%BA%A7%E5%BD%A9%E5%9B%BE%E9%A6%86.mobi)
* 富爸爸财富自由之路 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%AF%8C%E7%88%B8%E7%88%B8%E8%B4%A2%E5%AF%8C%E8%87%AA%E7%94%B1%E4%B9%8B%E8%B7%AF.mobi)
* 必然 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%BF%85%E7%84%B6.mobi)
* 终极复制 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%88%E6%9E%81%E5%A4%8D%E5%88%B6.mobi)
* 终极算法机器学习和人工智能如何重塑世界 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%BB%88%E6%9E%81%E7%AE%97%E6%B3%95%E6%9C%BA%E5%99%A8%E5%AD%A6%E4%B9%A0%E5%92%8C%E4%BA%BA%E5%B7%A5%E6%99%BA%E8%83%BD%E5%A6%82%E4%BD%95%E9%87%8D%E5%A1%91%E4%B8%96%E7%95%8C.mobi)
* 超级技术：改变未来社会和商业的技术趋势 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E7%BA%A7%E6%8A%80%E6%9C%AF%EF%BC%9A%E6%94%B9%E5%8F%98%E6%9C%AA%E6%9D%A5%E7%A4%BE%E4%BC%9A%E5%92%8C%E5%95%86%E4%B8%9A%E7%9A%84%E6%8A%80%E6%9C%AF%E8%B6%8B%E5%8A%BF.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E7%BA%A7%E6%8A%80%E6%9C%AF%EF%BC%9A%E6%94%B9%E5%8F%98%E6%9C%AA%E6%9D%A5%E7%A4%BE%E4%BC%9A%E5%92%8C%E5%95%86%E4%B8%9A%E7%9A%84%E6%8A%80%E6%9C%AF%E8%B6%8B%E5%8A%BF.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E8%B6%85%E7%BA%A7%E6%8A%80%E6%9C%AF%EF%BC%9A%E6%94%B9%E5%8F%98%E6%9C%AA%E6%9D%A5%E7%A4%BE%E4%BC%9A%E5%92%8C%E5%95%86%E4%B8%9A%E7%9A%84%E6%8A%80%E6%9C%AF%E8%B6%8B%E5%8A%BF.azw3)
* 亲爱的生活 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%B2%E7%88%B1%E7%9A%84%E7%94%9F%E6%B4%BB.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%B2%E7%88%B1%E7%9A%84%E7%94%9F%E6%B4%BB.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%B2%E7%88%B1%E7%9A%84%E7%94%9F%E6%B4%BB.azw3)
* 活出人生最好的可能 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%B4%BB%E5%87%BA%E4%BA%BA%E7%94%9F%E6%9C%80%E5%A5%BD%E7%9A%84%E5%8F%AF%E8%83%BD.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%B4%BB%E5%87%BA%E4%BA%BA%E7%94%9F%E6%9C%80%E5%A5%BD%E7%9A%84%E5%8F%AF%E8%83%BD.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%B4%BB%E5%87%BA%E4%BA%BA%E7%94%9F%E6%9C%80%E5%A5%BD%E7%9A%84%E5%8F%AF%E8%83%BD.azw3)
* 生命不息，折腾不止 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%94%9F%E5%91%BD%E4%B8%8D%E6%81%AF%EF%BC%8C%E6%8A%98%E8%85%BE%E4%B8%8D%E6%AD%A2.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%94%9F%E5%91%BD%E4%B8%8D%E6%81%AF%EF%BC%8C%E6%8A%98%E8%85%BE%E4%B8%8D%E6%AD%A2.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%94%9F%E5%91%BD%E4%B8%8D%E6%81%AF%EF%BC%8C%E6%8A%98%E8%85%BE%E4%B8%8D%E6%AD%A2.azw3)
* 看，那些小丑！ [mobi](https://oss-blog.cdn.hujingnb.com/book/%E7%9C%8B%EF%BC%8C%E9%82%A3%E4%BA%9B%E5%B0%8F%E4%B8%91%EF%BC%81.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E7%9C%8B%EF%BC%8C%E9%82%A3%E4%BA%9B%E5%B0%8F%E4%B8%91%EF%BC%81.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E7%9C%8B%EF%BC%8C%E9%82%A3%E4%BA%9B%E5%B0%8F%E4%B8%91%EF%BC%81.azw3)
* 智慧未来 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E6%99%BA%E6%85%A7%E6%9C%AA%E6%9D%A5.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E6%99%BA%E6%85%A7%E6%9C%AA%E6%9D%A5.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E6%99%BA%E6%85%A7%E6%9C%AA%E6%9D%A5.azw3)
* 好吗好的 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E5%A5%BD%E5%90%97%E5%A5%BD%E7%9A%84.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E5%A5%BD%E5%90%97%E5%A5%BD%E7%9A%84.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E5%A5%BD%E5%90%97%E5%A5%BD%E7%9A%84.azw3)
* 脑洞 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E8%84%91%E6%B4%9E.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E8%84%91%E6%B4%9E.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E8%84%91%E6%B4%9E.azw3)
* 人的脑洞略大于整个宇宙 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E7%9A%84%E8%84%91%E6%B4%9E%E7%95%A5%E5%A4%A7%E4%BA%8E%E6%95%B4%E4%B8%AA%E5%AE%87%E5%AE%99.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E7%9A%84%E8%84%91%E6%B4%9E%E7%95%A5%E5%A4%A7%E4%BA%8E%E6%95%B4%E4%B8%AA%E5%AE%87%E5%AE%99.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BA%BA%E7%9A%84%E8%84%91%E6%B4%9E%E7%95%A5%E5%A4%A7%E4%BA%8E%E6%95%B4%E4%B8%AA%E5%AE%87%E5%AE%99.azw3)
* 你也是蘑菇吗 [mobi](https://oss-blog.cdn.hujingnb.com/book/%E4%BD%A0%E4%B9%9F%E6%98%AF%E8%98%91%E8%8F%87%E5%90%97.mobi) [epub](https://oss-blog.cdn.hujingnb.com/book/%E4%BD%A0%E4%B9%9F%E6%98%AF%E8%98%91%E8%8F%87%E5%90%97.epub) [azw3](https://oss-blog.cdn.hujingnb.com/book/%E4%BD%A0%E4%B9%9F%E6%98%AF%E8%98%91%E8%8F%87%E5%90%97.azw3)










---

# 格式转换

## 转官方格式

对于`epub`格式的电子书, 使用`usb`传到**kindle**上是无法直接查看的, 如果遇到了, 可以下载亚马逊官方的一款软件进行格式转换. 

下载地址: [https://www.amazon.com/gp/feature.html/?t=joyo01s-20&docId=1003018611&tag=joyo01s-20](https://www.amazon.com/gp/feature.html/?t=joyo01s-20&docId=1003018611&tag=joyo01s-20)

**打开电子书**

![image-20210609224709472](https://oss-blog.cdn.hujingnb.com/img/20210609224709.png)

推动`epub`格式文件后, 稍作等待. 

**导出电子书**

![image-20210609225044255](https://oss-blog.cdn.hujingnb.com/img/20210609225044.png)

转换成功后即可导出. 

![image-20210609225149434](https://oss-blog.cdn.hujingnb.com/img/20210609225149.png)

如图是它支持导出的文件格式. 

## 转 azw3

有时, 通过官方工具转成`mobi`之后, 格式就乱了. 这时可以转成`azw3`格式查看, kindle 也是支持的. 这就简单多了, 很多在线工具网站都支持. 

比如: [https://epub2kindle.com/zh/](https://epub2kindle.com/zh/)

